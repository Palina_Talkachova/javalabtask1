package com.epam.newsmanagement.service.impl;

import static org.hamcrest.Matchers.*;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.when;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.runners.MockitoJUnitRunner;
import org.mockito.stubbing.Answer;

import com.epam.newsmanagement.entity.Author;
import com.epam.newsmanagement.entity.Comment;
import com.epam.newsmanagement.entity.News;
import com.epam.newsmanagement.entity.NewsVO;
import com.epam.newsmanagement.entity.Tag;
import com.epam.newsmanagement.service.AuthorService;
import com.epam.newsmanagement.service.CommentService;
import com.epam.newsmanagement.service.NewsService;
import com.epam.newsmanagement.service.TagService;
import com.epam.newsmanagement.service.exception.ServiceException;
import com.epam.newsmanagement.service.impl.NewsHandlingServiceImpl;

/**
 * The Class NewsHandlingServiceImplTest.
 */
@RunWith(MockitoJUnitRunner.class)
public class NewsHandlingServiceImplTest {
    @Mock
    private NewsService newsServiceMock;
    @Mock
    private AuthorService authorServiceMock;
    @Mock
    private TagService tagServiceMock;
    @Mock
    private CommentService commentServiceMock;
    @InjectMocks
    private NewsHandlingServiceImpl newsHandlingService;
    private News news;
    private Author author;
    private List<Tag> tags;
    private List<Comment> comments;

    /**
     * Setup date
     *
     * @throws ServiceException
     *             the service exception
     */
    @Before
    public void setupData() throws ServiceException {
	when(newsServiceMock.takeById(any(Long.class))).thenAnswer(new Answer<News>() {
	    @Override
	    public News answer(InvocationOnMock invocation) throws Throwable {
		news.setNewsId((Long) invocation.getArguments()[0]);
		return news;
	    }
	});
	when(authorServiceMock.takeByNewsId(any(Long.class))).thenAnswer(new Answer<Author>() {
	    @Override
	    public Author answer(InvocationOnMock invocation) throws Throwable {
		author.setAuthorId((Long) invocation.getArguments()[0]);
		return author;
	    }
	});
	when(tagServiceMock.takeByNewsId(any(Long.class))).thenAnswer(new Answer<List<Tag>>() {
	    @Override
	    public List<Tag> answer(InvocationOnMock invocation) throws Throwable {
		return tags;
	    }
	});
	when(commentServiceMock.takeByNewsId(any(Long.class))).thenAnswer(new Answer<List<Comment>>() {
	    @Override
	    public List<Comment> answer(InvocationOnMock invocation) throws Throwable {
		for (Comment comment : comments) {
		    comment.setCommentId((Long) invocation.getArguments()[0]);
		}
		return comments;
	    }
	});
    }

    /**
     * Setup objects.
     */
    @Before
    public void setCompletedNewsInfo() {
	news = new News();
	news.setNewsId(1L);
	news.setTitle("Title");
	news.setShortText("Short text");
	news.setFullText("Full text");
	news.setCreationDate(Timestamp.valueOf("2015-09-09 10:10:30.11"));
	author = new Author();
	author.setAuthorId(1L);
	author.setAuthorName("Jecky");
	tags = new ArrayList<>();
	Tag tagOne = new Tag();
	tagOne.setTagId(1L);
	tagOne.setTagName("sport");
	Tag tagTwo = new Tag();
	tagTwo.setTagId(2L);
	tagTwo.setTagName("culture");
	tags.add(tagOne);
	tags.add(tagTwo);
	comments = new ArrayList<>();
	Comment commentOne = new Comment();
	commentOne.setCommentId(1L);
	commentOne.setCommentText("Great");
	commentOne.setCreationDate(Timestamp.valueOf("2015-10-10 10:10:30.10"));
	Comment commentTwo = new Comment();
	commentTwo.setCommentId(2L);
	commentTwo.setCommentText("Fine");
	commentTwo.setCreationDate(Timestamp.valueOf("2015-08-08 10:10:30.09"));
	comments.add(commentOne);
	comments.add(commentTwo);
    }

    /**
     * Take single news.
     *
     * @throws ServiceException
     *             the service exception
     */
    @Test
    public void takeCompletedNews() throws ServiceException {
	NewsVO newsVO = newsHandlingService.takeCompletedNews(1L);
	assertEquals(news, newsVO.getNews());
	assertEquals(author, newsVO.getAuthor());
	assertEquals(tags, newsVO.getTags());
	assertEquals(comments, newsVO.getComments());
    }

    /**
     * Save news.
     *
     * @throws ServiceException
     *             the service exception
     */
    @Test
    public void saveNews() throws ServiceException {
	List<Long> tagList = new ArrayList<>();
	for (Tag tag : tags) {
	    tagList.add(tag.getTagId());
	}
	newsHandlingService.saveNews(news, author.getAuthorId(), tagList);
	assertThat(news.getNewsId(), is(1L));
    }
}
