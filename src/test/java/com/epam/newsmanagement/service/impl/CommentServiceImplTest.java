package com.epam.newsmanagement.service.impl;

import static org.hamcrest.Matchers.*;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.runners.MockitoJUnitRunner;
import org.mockito.stubbing.Answer;

import com.epam.newsmanagement.dao.CommentDao;
import com.epam.newsmanagement.dao.exception.DaoException;
import com.epam.newsmanagement.entity.Comment;
import com.epam.newsmanagement.service.exception.ServiceException;
import com.epam.newsmanagement.service.impl.CommentServiceImpl;

/**
 * The Class CommentServiceImplTest.
 */
@RunWith(MockitoJUnitRunner.class)
public class CommentServiceImplTest {
    @Mock
    private CommentDao commentDaoMock;
    @InjectMocks
    private CommentServiceImpl commentService;

    /**
     * Do setup.
     *
     * @throws DaoException
     *             the dao exception
     */
    @Before
    public void setupData() throws DaoException {
	when(commentDaoMock.takeById(any(Long.class))).thenAnswer(new Answer<Comment>() {
	    public Comment answer(InvocationOnMock invocation) throws Throwable {
		Comment comment = new Comment();
		comment.setCommentId(1L);
		comment.setNewsId(((Long) invocation.getArguments()[0]));
		comment.setCommentText("Greate");
		return comment;
	    }
	});
	when(commentDaoMock.takeAll()).thenAnswer(new Answer<List<Comment>>() {
	    @Override
	    public List<Comment> answer(InvocationOnMock invocation) throws Throwable {
		List<Comment> comments = new ArrayList<>();
		comments.add(new Comment());
		comments.add(new Comment());
		return comments;
	    }
	});
	when(commentDaoMock.takeByNewsId(any(Long.class))).thenAnswer(new Answer<List<Comment>>() {
	    @Override
	    public List<Comment> answer(InvocationOnMock invocation) throws Throwable {
		List<Comment> comments = new ArrayList<>();
		Comment comment = new Comment();
		comment.setCommentId(3L);
		comment.setNewsId(1L);
		comment.setCommentText("So good!");
		comments.add(comment);
		return comments;
	    }
	});
    }

    /**
     * Adds the comment.
     *
     * @throws ServiceException
     *             the service exception
     * @throws DaoException
     *             the dao exception
     */
    @Test
    public void addComment() throws ServiceException, DaoException {
	Comment comment = new Comment();
	comment.setCommentId(1L);
	comment.setNewsId(1L);
	comment.setCommentText("Fine");
	commentService.add(comment);
	verify(commentDaoMock).add(comment);
    }

    /**
     * Take by id.
     *
     * @throws ServiceException
     *             the service exception
     */
    @Test
    public void takeById() throws ServiceException {
	Comment comment = new Comment();
	comment.setCommentId(1L);
	comment.setNewsId(1L);
	comment.setCommentText("Greate");
	assertEquals(comment, commentService.takeById(1L));
    }

    /**
     * Update comment.
     *
     * @throws ServiceException
     *             the service exception
     * @throws DaoException
     *             the dao exception
     */
    @Test
    public void updateComment() throws ServiceException, DaoException {
	Comment comment = new Comment();
	comment.setCommentId(1L);
	comment.setCommentText("Not good");
	commentService.update(comment);
	verify(commentDaoMock).update(comment);
    }

    /**
     * Delete by id.
     *
     * @throws ServiceException
     *             the service exception
     * @throws DaoException
     *             the dao exception
     */
    @Test
    public void deleteById() throws ServiceException, DaoException {
	Long id = 1L;
	commentService.deleteById(id);
	verify(commentDaoMock).deleteById(id);
    }

    /**
     * Take all comments.
     *
     * @throws ServiceException
     *             the service exception
     */
    @Test
    public void takeAllComments() throws ServiceException {
	List<Comment> comments = commentService.takeAll();
	assertThat(comments.size(), is(2));
    }

    /**
     * Take by news id.
     *
     * @throws ServiceException
     *             the service exception
     */
    @Test
    public void takeByNewsId() throws ServiceException {
	List<Comment> comments = commentService.takeByNewsId(2L);
	assertThat(comments.size(), is(1));
    }
}
