package com.epam.newsmanagement.service.impl;

import static org.hamcrest.Matchers.*;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertThat;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.runners.MockitoJUnitRunner;
import org.mockito.stubbing.Answer;

import com.epam.newsmanagement.dao.AuthorDao;
import com.epam.newsmanagement.dao.exception.DaoException;
import com.epam.newsmanagement.entity.Author;
import com.epam.newsmanagement.service.exception.ServiceException;
import com.epam.newsmanagement.service.impl.AuthorServiceImpl;

/**
 * The Class AuthorServiceImplTest.
 */
@RunWith(MockitoJUnitRunner.class)
public class AuthorServiceImplTest {
    @Mock
    private AuthorDao authorDaoMock;
    @InjectMocks
    private AuthorServiceImpl authorService;

    /**
     * Do setup.
     *
     * @throws DaoException
     *             the dao exception
     */
    @Before
    public void setupData() throws DaoException {
	when(authorDaoMock.takeById(any(Long.class))).thenAnswer(new Answer<Author>() {
	    @Override
	    public Author answer(InvocationOnMock invocation) throws Throwable {
		Author author = new Author();
		author.setAuthorId(((Long) invocation.getArguments()[0]));
		author.setAuthorName("Kelly");
		return author;
	    }
	});
	when(authorDaoMock.takeAll()).thenAnswer(new Answer<List<Author>>() {
	    @Override
	    public List<Author> answer(InvocationOnMock invocation) throws Throwable {
		List<Author> authors = new ArrayList<>();
		authors.add(new Author());
		authors.add(new Author());
		authors.add(new Author());
		return authors;
	    }
	});
	when(authorDaoMock.takeByNewsId(any(Long.class))).thenAnswer(new Answer<Author>() {
	    @Override
	    public Author answer(InvocationOnMock invocation) throws Throwable {
		return new Author();
	    }
	});
    }

    /**
     * Adds the author.
     *
     * @throws ServiceException
     *             the service exception
     * @throws DaoException
     *             the dao exception
     */
    @Test
    public void addAuthor() throws ServiceException, DaoException {
	Author author = new Author();
	author.setAuthorId(1L);
	author.setAuthorName("Kevin");
	authorService.add(author);
	verify(authorDaoMock).add(author);
    }

    /**
     * Take by id.
     *
     * @throws ServiceException
     *             the service exception
     */
    @Test
    public void takeById() throws ServiceException {
	Author author = new Author();
	author.setAuthorId(1L);
	author.setAuthorName("Kelly");
	assertEquals(author, authorService.takeById(1L));
    }

    /**
     * Update author.
     *
     * @throws ServiceException
     *             the service exception
     * @throws DaoException
     *             the dao exception
     */
    @Test
    public void updateAuthor() throws ServiceException, DaoException {
	Author author = new Author();
	author.setAuthorId(1L);
	author.setAuthorName("Paulo");
	authorService.update(author);
	verify(authorDaoMock).update(author);
    }

    /**
     * Expire author.
     *
     * @throws ServiceException
     *             the service exception
     * @throws DaoException
     *             the dao exception
     */
    @Test
    public void expireAuthor() throws ServiceException, DaoException {
	Author author = new Author();
	author.setAuthorId(1L);
	author.setAuthorName("John");
	author.setExpired(Timestamp.valueOf("2015-09-09 10:10:30.11"));
	authorService.expireAuthor(author);
	verify(authorDaoMock).expireAuthor(author);
    }

    /**
     * Take all authors.
     *
     * @throws ServiceException
     *             the service exception
     */
    @Test
    public void takeAllAuthors() throws ServiceException {
	List<Author> authors = authorService.takeAll();
	assertThat(authors.size(), is(3));
    }

    /**
     * Take by news id.
     *
     * @throws ServiceException
     *             the service exception
     */
    @Test
    public void takeByNewsId() throws ServiceException {
	Author author = authorService.takeByNewsId(1L);
	assertNotNull(author);
    }
}
