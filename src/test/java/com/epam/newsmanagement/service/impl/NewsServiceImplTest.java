/*
 * 
 */
package com.epam.newsmanagement.service.impl;

import static org.hamcrest.Matchers.*;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.sql.Date;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.runners.MockitoJUnitRunner;
import org.mockito.stubbing.Answer;

import com.epam.newsmanagement.dao.NewsDao;
import com.epam.newsmanagement.dao.exception.DaoException;
import com.epam.newsmanagement.entity.News;
import com.epam.newsmanagement.entity.SearchCriteria;
import com.epam.newsmanagement.service.exception.ServiceException;
import com.epam.newsmanagement.service.impl.NewsServiceImpl;

/**
 * The Class NewsServiceImplTest.
 */
@RunWith(MockitoJUnitRunner.class)
public class NewsServiceImplTest {
    @Mock
    private NewsDao newsDaoMock;
    @InjectMocks
    private NewsServiceImpl newsService;

    /**
     * Do setup.
     *
     * @throws DaoException
     *             the dao exception
     */
    @Before
    public void setupData() throws DaoException {
	when(newsDaoMock.takeById(any(Long.class))).thenAnswer(new Answer<News>() {
	    @Override
	    public News answer(InvocationOnMock invocation) throws Throwable {
		News news = new News();
		news.setNewsId((Long) invocation.getArguments()[0]);
		news.setTitle("Title");
		news.setShortText("Short text");
		news.setFullText("Full text");
		news.setCreationDate(Timestamp.valueOf("2015-10-11 07:11:11.07"));
		news.setModificationDate(Date.valueOf("2015-10-12"));
		return news;
	    }
	});
	when(newsDaoMock.takeAll()).thenAnswer(new Answer<List<News>>() {
	    @Override
	    public List<News> answer(InvocationOnMock invocation) throws Throwable {
		List<News> news = new ArrayList<>();
		news.add(new News());
		news.add(new News());
		news.add(new News());
		return news;
	    }
	});
	when(newsDaoMock.countAllNews()).thenAnswer(new Answer<Long>() {
	    @Override
	    public Long answer(InvocationOnMock invocation) throws Throwable {
		List<News> news = new ArrayList<>();
		news.add(new News());
		news.add(new News());
		Long size = (long) news.size();
		return size;
	    }
	});
	when(newsDaoMock.takeByAuhtorId(any(Long.class))).thenAnswer(new Answer<List<News>>() {
	    @Override
	    public List<News> answer(InvocationOnMock invocation) throws Throwable {
		List<News> newsList = new ArrayList<>();
		News news = new News();
		news.setNewsId(1L);
		news.setTitle("Title");
		news.setShortText("Short text");
		news.setFullText("Full text");
		news.setCreationDate(Timestamp.valueOf("2015-10-11 07:11:11.07"));
		news.setModificationDate(Date.valueOf("2015-10-12"));
		newsList.add(news);
		return newsList;
	    }
	});
	when(newsDaoMock.findNewsBySearchCriteria(any(SearchCriteria.class))).thenAnswer(new Answer<List<News>>() {
	    @Override
	    public List<News> answer(InvocationOnMock invocation) throws Throwable {
		List<News> news = new ArrayList<>();
		news.add(new News());
		return news;
	    }
	});
	when(newsDaoMock.takeByTagId(any(Long.class))).thenAnswer(new Answer<List<News>>() {
	    @Override
	    public List<News> answer(InvocationOnMock invocation) throws Throwable {
		List<News> news = new ArrayList<>();
		news.add(new News());
		return news;
	    }
	});
    }

    /**
     * Adds the news.
     *
     * @throws ServiceException
     *             the service exception
     * @throws DaoException
     *             the dao exception
     */
    @Test
    public void addNews() throws ServiceException, DaoException {
	News news = new News();
	news.setNewsId(1L);
	news.setTitle("Some title");
	news.setShortText("Some short text");
	news.setFullText("Some full text");
	news.setCreationDate(Timestamp.valueOf("2015-10-10 07:11:11.10"));
	newsService.add(news);
	verify(newsDaoMock).add(news);
    }

    /**
     * Take by id.
     *
     * @throws ServiceException
     *             the service exception
     */
    @Test
    public void takeById() throws ServiceException {
	News news = new News();
	news.setNewsId(1L);
	news.setTitle("Title");
	news.setShortText("Short text");
	news.setFullText("Full text");
	news.setCreationDate(Timestamp.valueOf("2015-10-11 07:11:11.07"));
	news.setModificationDate(Date.valueOf("2015-10-12"));
	assertEquals(news, newsService.takeById(1L));
    }

    /**
     * Update news.
     *
     * @throws ServiceException
     *             the service exception
     * @throws DaoException
     *             the dao exception
     */
    @Test
    public void updateNews() throws ServiceException, DaoException {
	News news = new News();
	news.setNewsId(1L);
	news.setTitle("Title");
	news.setShortText("The short text");
	news.setFullText("The full text");
	news.setCreationDate(Timestamp.valueOf("2015-11-11 11:11:11.07"));
	newsService.update(news);
	verify(newsDaoMock).update(news);
    }

    /**
     * Delete news.
     *
     * @throws ServiceException
     *             the service exception
     * @throws DaoException
     *             the dao exception
     */
    @Test
    public void deleteNews() throws ServiceException, DaoException {
	Long id = 1L;
	newsService.deleteById(id);
	verify(newsDaoMock).deleteById(id);
    }

    /**
     * Take all news.
     *
     * @throws ServiceException
     *             the service exception
     */
    @Test
    public void takeAllNews() throws ServiceException {
	List<News> newsList = newsService.takeAll();
	assertThat(newsList.size(), is(3));
    }

    /**
     * Take by author id.
     *
     * @throws ServiceException
     *             the service exception
     */
    @Test
    public void takeByAuthorId() throws ServiceException {
	Long newsId = 1L;
	Long authorId = 1L;
	newsService.addNewsAuthor(newsId, authorId);
	List<News> newslist = newsService.takeByAuhtorId(1L);
	assertThat(newslist.size(), is(1));
    }

    /**
     * Take by tag id.
     *
     * @throws ServiceException
     *             the service exception
     */
    @Test
    public void takeByTagId() throws ServiceException {
	List<News> news = newsService.takeByTagId(1L);
	assertThat(news.size(), is(1));
    }

    /**
     * Adds the news author.
     *
     * @throws ServiceException
     *             the service exception
     * @throws DaoException
     *             the dao exception
     */
    @Test
    public void addNewsAuthor() throws ServiceException, DaoException {
	Long newsId = 1L;
	Long authorId = 1L;
	newsService.addNewsAuthor(newsId, authorId);
	verify(newsDaoMock).addNewsAuthor(newsId, authorId);
    }

    /**
     * Adds the news tag.
     *
     * @throws ServiceException
     *             the service exception
     * @throws DaoException
     *             the dao exception
     */
    @Test
    public void addNewsTag() throws ServiceException, DaoException {
	Long newsId = 1L;
	Long tagIdOne = 1L;
	Long tagIdTwo = 2L;
	List<Long> tagsIdList = new ArrayList<>();
	tagsIdList.add(tagIdOne);
	tagsIdList.add(tagIdTwo);
	newsService.addNewsTag(newsId, tagsIdList);
	verify(newsDaoMock).addNewsTagList(newsId, tagsIdList);
    }

    /**
     * Count all news.
     *
     * @throws ServiceException
     *             the service exception
     */
    @Test
    public void countAllNews() throws ServiceException {
	Long newsQuantity = newsService.countAllNews();
	assertThat(newsQuantity, is(2L));
    }
}