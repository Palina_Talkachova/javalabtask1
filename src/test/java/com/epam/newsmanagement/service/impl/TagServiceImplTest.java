/*
 * 
 */
package com.epam.newsmanagement.service.impl;

import static org.hamcrest.Matchers.*;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.runners.MockitoJUnitRunner;
import org.mockito.stubbing.Answer;

import com.epam.newsmanagement.dao.TagDao;
import com.epam.newsmanagement.dao.exception.DaoException;
import com.epam.newsmanagement.entity.Tag;
import com.epam.newsmanagement.service.exception.ServiceException;
import com.epam.newsmanagement.service.impl.TagServiceImpl;

/**
 * The Class TagServiceImplTest.
 */
@RunWith(MockitoJUnitRunner.class)
public class TagServiceImplTest {
    @Mock
    private TagDao tagDaoMock;
    @InjectMocks
    private TagServiceImpl tagService;

    /**
     * Do setup.
     *
     * @throws DaoException
     *             the dao exception
     */
    @Before
    public void setupData() throws DaoException {
	when(tagDaoMock.takeById(any(Long.class))).thenAnswer(new Answer<Tag>() {
	    public Tag answer(InvocationOnMock invocation) throws Throwable {
		Tag tag = new Tag();
		tag.setTagId(((Long) invocation.getArguments()[0]));
		tag.setTagName("culture");
		return tag;
	    }
	});
	when(tagDaoMock.takeAll()).thenAnswer(new Answer<List<Tag>>() {
	    @Override
	    public List<Tag> answer(InvocationOnMock invocation) throws Throwable {
		List<Tag> tags = new ArrayList<>();
		tags.add(new Tag());
		tags.add(new Tag());
		tags.add(new Tag());
		return tags;
	    }
	});
	when(tagDaoMock.takeByNewsId(any(Long.class))).thenAnswer(new Answer<List<Tag>>() {
	    @Override
	    public List<Tag> answer(InvocationOnMock invocation) throws Throwable {
		List<Tag> authors = new ArrayList<>();
		authors.add(new Tag());
		return authors;
	    }
	});
    }

    /**
     * Adds the tag.
     *
     * @throws ServiceException
     *             the service exception
     * @throws DaoException
     *             the dao exception
     */
    @Test
    public void addTag() throws ServiceException, DaoException {
	Tag tag = new Tag();
	tag.setTagId(2L);
	tag.setTagName("sport");
	tagService.add(tag);
	verify(tagDaoMock).add(tag);
    }

    /**
     * Take by id.
     *
     * @throws ServiceException
     *             the service exception
     * @throws DaoException
     *             the dao exception
     */
    @Test
    public void takeById() throws ServiceException, DaoException {
	Tag tag = new Tag();
	tag.setTagId(1L);
	tag.setTagName("culture");
	assertEquals(tag, tagDaoMock.takeById(1L));
    }

    /**
     * Update tag.
     *
     * @throws ServiceException
     *             the service exception
     * @throws DaoException
     *             the dao exception
     */
    @Test
    public void updateTag() throws ServiceException, DaoException {
	Tag tag = new Tag();
	tag.setTagId(1L);
	tag.setTagName("music");
	tagService.update(tag);
	verify(tagDaoMock).update(tag);
    }

    /**
     * Delete by id.
     *
     * @throws ServiceException
     *             the service exception
     * @throws DaoException
     *             the dao exception
     */
    @Test
    public void deleteById() throws ServiceException, DaoException {
	Long id = 1L;
	tagService.deleteById(id);
	verify(tagDaoMock).deleteById(id);
    }

    /**
     * Take all tags.
     *
     * @throws ServiceException
     *             the service exception
     */
    @Test
    public void takeAllTags() throws ServiceException {
	List<Tag> tags = tagService.takeAll();
	assertThat(tags.size(), is(3));
    }
}
