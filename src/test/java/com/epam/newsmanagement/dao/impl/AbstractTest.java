package com.epam.newsmanagement.dao.impl;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.SQLException;

import javax.sql.DataSource;

import org.dbunit.DatabaseUnitException;
import org.dbunit.database.DatabaseConfig;
import org.dbunit.database.DatabaseConnection;
import org.dbunit.database.IDatabaseConnection;
import org.dbunit.dataset.DataSetException;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.xml.FlatXmlDataSetBuilder;
import org.dbunit.ext.oracle.OracleDataTypeFactory;
import org.dbunit.operation.DatabaseOperation;
import org.junit.After;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.datasource.DataSourceUtils;

/**
 * The Class AbstractTest.
 */
public abstract class AbstractTest {
    @Autowired
    DataSource dataSource;
    Connection con;

    /**
     * gets a connection.
     *
     * @return the connection
     * @throws Exception
     *             the exception
     */
    public IDatabaseConnection takeConnection() throws Exception {
	con = DataSourceUtils.getConnection(dataSource);
	DatabaseMetaData databaseMetaData = con.getMetaData();
	IDatabaseConnection connection = new DatabaseConnection(con, databaseMetaData.getUserName().toUpperCase());
	connection.getConfig().setProperty(DatabaseConfig.PROPERTY_DATATYPE_FACTORY, new OracleDataTypeFactory());
	return connection;
    }

    /**
     * gets a data set for inserting to the test DB.
     *
     * @return the data set for inserting
     * @throws DataSetException
     *             the data set exception
     * @throws FileNotFoundException
     *             the file not found exception
     */
    public abstract IDataSet takeDataSetToInsert() throws DataSetException, FileNotFoundException;

    /**
     * gets a data set for deleting from the test DB.
     *
     * @return the data set for deleting
     * @throws DataSetException
     *             the data set exception
     * @throws FileNotFoundException
     *             the file not found exception
     */
    public IDataSet takeDataSetToDelete() throws DataSetException, FileNotFoundException {
	return new FlatXmlDataSetBuilder().build(new FileInputStream("src/test/resources/delete_data.xml"));
    }

    /**
     * configures a test case for the text DB.
     *
     * @throws DatabaseUnitException
     *             the database unit exception
     * @throws SQLException
     *             the SQL exception
     * @throws Exception
     *             the exception
     */
    public abstract void configureDBTestCase() throws DatabaseUnitException, SQLException, Exception;

    /**
     * cleans the test DB from inserted data
     *
     * @throws DatabaseUnitException
     *             the database unit exception
     * @throws SQLException
     *             the SQL exception
     * @throws Exception
     *             the exception
     */
    @After
    public void cleanDataBase() throws DatabaseUnitException, SQLException, Exception {
	DatabaseOperation.DELETE_ALL.execute(takeConnection(), takeDataSetToDelete());
	releaseConnection();
    }

    @After
    public void releaseConnection() {
	DataSourceUtils.releaseConnection(con, dataSource);
    }
}
