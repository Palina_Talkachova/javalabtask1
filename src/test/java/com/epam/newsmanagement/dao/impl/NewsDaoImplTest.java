package com.epam.newsmanagement.dao.impl;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertThat;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.sql.Date;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import org.dbunit.dataset.CompositeDataSet;
import org.dbunit.dataset.DataSetException;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.xml.FlatXmlDataSetBuilder;
import org.dbunit.operation.DatabaseOperation;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.epam.newsmanagement.config.ApplicationTestConfig;
import com.epam.newsmanagement.dao.NewsDao;
import com.epam.newsmanagement.dao.exception.DaoException;
import com.epam.newsmanagement.entity.News;
import com.epam.newsmanagement.entity.SearchCriteria;

/**
 * The Class NewsDaoTest.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = { ApplicationTestConfig.class })
public class NewsDaoImplTest extends AbstractTest {
    @Autowired
    private NewsDao newsDao;

    /**
     * 
     * @see com.epam.newsmanagement.dao.AbstractTest#takeDataSetToInsert()
     */
    @Override
    public IDataSet takeDataSetToInsert() throws DataSetException, FileNotFoundException {
	IDataSet[] datasets = new IDataSet[] {
		new FlatXmlDataSetBuilder().build(new FileInputStream("src/test/resources/news_data.xml")),
		new FlatXmlDataSetBuilder().build(new FileInputStream("src/test/resources/comment_data.xml")),
		new FlatXmlDataSetBuilder().build(new FileInputStream("src/test/resources/author_data.xml")),
		new FlatXmlDataSetBuilder().build(new FileInputStream("src/test/resources/tag_data.xml")),
		new FlatXmlDataSetBuilder().build(new FileInputStream("src/test/resources/news_author_data.xml")),
		new FlatXmlDataSetBuilder().build(new FileInputStream("src/test/resources/news_tag_data.xml")) };
	return new CompositeDataSet(datasets);
    }

    /**
     * 
     * @see com.epam.newsmanagement.dao.AbstractTest#configureDBTestCase()
     */
    @Override
    @Before
    public void configureDBTestCase() throws FileNotFoundException, Exception {
	DatabaseOperation.CLEAN_INSERT.execute(takeConnection(), takeDataSetToInsert());
    }

    /**
     * Adds news.
     *
     * @throws DaoException
     *             the dao exception
     */
    @Test
    public void addNews() throws DaoException {
	News news = new News();
	news.setTitle("Title");
	news.setShortText("Short text");
	news.setFullText("Full text");
	news.setCreationDate(Timestamp.valueOf("2015-09-09 10:10:30.05"));
	news.setModificationDate(Date.valueOf("2015-10-10"));
	long newsId = newsDao.add(news);
	assertNotEquals(0, newsId);
	assertNotNull(newsDao.takeById(newsId));
    }
    
    /**
     * Deletes news.
     *
     * @throws DaoException
     *             the dao exception
     */
    @Test
    public void deleteNews() throws DaoException {
	News news = new News();
	news.setNewsId(3L);
	news.setTitle("Title");
	news.setShortText("Short text");
	news.setFullText("Full text");
	news.setCreationDate(Timestamp.valueOf("2015-09-09 10:10:30.05"));
	news.setModificationDate(Date.valueOf("2015-10-10"));
	long newsId = newsDao.add(news);
	assertNotEquals(0, newsId);
	assertNotNull(newsDao.takeById(newsId));
	newsDao.deleteById(newsId);
	assertNull(newsDao.takeById(newsId));
    }


    /**
     * Take by id.
     *
     * @throws DaoException
     *             the dao exception
     */
    @Test
    public void takeById() throws DaoException {
	News news = new News();
	news.setNewsId(1L);
	news.setTitle("Title");
	news.setShortText("Short text");
	news.setFullText("Full text");
	news.setCreationDate(Timestamp.valueOf("2015-09-09 10:10:30.10"));
	news.setModificationDate(Date.valueOf("2015-10-11"));
	assertEquals(news, newsDao.takeById(1L));
    }

    /**
     * Update news.
     *
     * @throws DaoException
     *             the dao exception
     */
    @Test
    public void updateNews() throws DaoException {
	News news = newsDao.takeById(1L);
	news.setShortText("New short text");
	newsDao.update(news);
	String shortText = newsDao.takeById(1L).getShortText();
	assertEquals("New short text", shortText);
    }

    /**
     * Take all news.
     *
     * @throws DaoException
     *             the dao exception
     */
    @Test
    public void takeAllNews() throws DaoException {
	List<News> newsList = newsDao.takeAll();
	assertThat(newsList.size(), is(3));
    }

    /**
     * Take by author id.
     *
     * @throws DaoException
     *             the dao exception
     */
    @Test
    public void takeByAuthorId() throws DaoException {
	List<News> newsList = newsDao.takeByAuhtorId(1L);
	assertThat(newsList.size(), is(2));
    }

    /**
     * Take by tag id.
     *
     * @throws DaoException
     *             the dao exception
     */
    @Test
    public void takeByTagId() throws DaoException {
	List<News> newsList = newsDao.takeByTagId(1L);
	assertThat(newsList.size(), is(2));
    }

    /**
     * Find by a search criteria.
     *
     * @throws DaoException
     *             the dao exception
     */
    @Test
    public void findBySearchCriteria() throws DaoException {
	SearchCriteria searchCriteria = new SearchCriteria();
	searchCriteria.setAuthorId(1L);
	searchCriteria.addTagId(1L);
	searchCriteria.addTagId(2L);
	List<News> news = newsDao.findNewsBySearchCriteria(searchCriteria);
	assertThat(news.size(), is(2));
    }
    
    /**
     * Find sorted news by a search criteria.
     *
     * @throws DaoException
     *             the dao exception
     */
    @Test
    public void countNewsBySearchCriteria() throws DaoException {
	SearchCriteria searchCriteria = new SearchCriteria();
	searchCriteria.setAuthorId(1L);
	searchCriteria.addTagId(1L);
	searchCriteria.addTagId(2L);
	int newsQuantity = newsDao.findAndCountNewsBySearchCriteria(searchCriteria);
	assertThat(newsQuantity, is(2));
    }


    /**
     * Count all news.
     *
     * @throws DaoException
     *             the dao exception
     */
    @Test
    public void countAllNews() throws DaoException {
	Long newsQuantity = newsDao.countAllNews();
	assertThat(newsQuantity, is(3L));
    }
    
    @Test
    public void addNewsTag() throws DaoException {
	Long newsId = 1L;
	Long tagIdOne = 1L;
	Long tagIdTwo = 2L;
	List<Long> tagsIdList = new ArrayList<>();
	tagsIdList.add(tagIdOne);
	tagsIdList.add(tagIdTwo);
	newsDao.addNewsTagList(newsId, tagsIdList);
	assertThat(tagsIdList.size(), is(2));
    }
    
    @Test
    public void deleteCommentsByNewsIdList() throws DaoException {
	List<Long> newsIdList = new ArrayList<>();
	newsIdList.add(1L);
	newsIdList.add(2L);
	newsDao.deleteCommentsByNewsIdList(newsIdList);
    }
    
    @Test
    public void deleteNewsAuthorByNewsIdList() throws DaoException {
	List<Long> newsIdList = new ArrayList<>();
	newsIdList.add(1L);
	newsIdList.add(2L);
	newsDao.deleteNewsAuthorByNewsIdList(newsIdList);
    }
    
    @Test
    public void deleteNewsTagByNewsIdList() throws DaoException {
	List<Long> newsIdList = new ArrayList<>();
	newsIdList.add(1L);
	newsIdList.add(2L);
	newsDao.deleteNewsTagByNewsIdList(newsIdList);
    }
    
//    @Test
//    public void deleteNewsByNewsIdList() throws DaoException {
//	List<Long> newsIdList = new ArrayList<>();
//	newsIdList.add(1L);
//	newsIdList.add(2L);
//	newsDao.deleteNewsByNewsIdList(newsIdList);
//    }
}
