package com.epam.newsmanagement.dao.impl;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertThat;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.sql.Timestamp;
import java.util.List;

import org.dbunit.dataset.CompositeDataSet;
import org.dbunit.dataset.DataSetException;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.xml.FlatXmlDataSetBuilder;
import org.dbunit.operation.DatabaseOperation;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.epam.newsmanagement.config.ApplicationTestConfig;
import com.epam.newsmanagement.dao.CommentDao;
import com.epam.newsmanagement.dao.exception.DaoException;
import com.epam.newsmanagement.entity.Comment;

/**
 * The Class CommentDaoTest.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = { ApplicationTestConfig.class })
public class CommentDaoImplTest extends AbstractTest {
    @Autowired
    private CommentDao commentDao;

    /**
     * Take data set to insert.
     *
     * @return the i data set
     * @throws DataSetException
     *             the data set exception
     * @throws FileNotFoundException
     *             the file not found exception
     * @see com.epam.newsmanagement.dao.impl.news.dao.implementation.AbstractTest#takeDataSetToInsert()
     */
    @Override
    public IDataSet takeDataSetToInsert() throws DataSetException, FileNotFoundException {
	IDataSet[] datasets = new IDataSet[] {
		new FlatXmlDataSetBuilder().build(new FileInputStream("src/test/resources/news_data.xml")),
		new FlatXmlDataSetBuilder().build(new FileInputStream("src/test/resources/comment_data.xml")) };
	return new CompositeDataSet(datasets);
    }

    /**
     * Configure db test case.
     *
     * @throws FileNotFoundException
     *             the file not found exception
     * @throws Exception
     *             the exception
     * @see com.epam.newsmanagement.dao.impl.news.dao.implementation.AbstractTest#configureDBTestCase()
     */
    @Override
    @Before
    public void configureDBTestCase() throws FileNotFoundException, Exception {
	DatabaseOperation.CLEAN_INSERT.execute(takeConnection(), takeDataSetToInsert());
    }

    /**
     * Adds comment.
     *
     * @throws DaoException
     *             the dao exception
     */
    @Test
    public void addComment() throws DaoException {
	Comment comment = new Comment();
	comment.setNewsId(1L);
	comment.setCommentText("Text");
	long commentId = commentDao.add(comment);
	assertNotEquals(0, commentId);
	assertNotNull(commentDao.takeById(commentId));
    }

    /**
     * Deletes comment.
     *
     * @throws DaoException
     *             the dao exception
     */
    @Test
    public void deleteComment() throws DaoException {
	Comment comment = new Comment();
	comment.setNewsId(2L);
	comment.setCommentText("Text Comment");
	long commentId = commentDao.add(comment);
	assertNotEquals(0, commentId);
	assertNotNull(commentDao.takeById(commentId));
	commentDao.deleteById(commentId);
	assertNull(commentDao.takeById(commentId));
    }

    /**
     * Take by id.
     *
     * @throws DaoException
     *             the dao exception
     */
    @Test
    public void takeById() throws DaoException {
	Comment comment = new Comment();
	comment.setCommentId(1L);
	comment.setNewsId(1L);
	comment.setCommentText("Text1");
	comment.setCreationDate(Timestamp.valueOf("2015-09-10 10:10:30.12"));
	assertEquals(comment, commentDao.takeById(1L));
    }

    /**
     * Update comment.
     *
     * @throws DaoException
     *             the dao exception
     */
    @Test
    public void updateComment() throws DaoException {
	Comment comment = commentDao.takeById(1L);
	comment.setCommentText("Text");
	commentDao.update(comment);
	assertEquals("Text", commentDao.takeById(1L).getCommentText());
    }

    /**
     * Take all comments.
     *
     * @throws DaoException
     *             the dao exception
     */
    @Test
    public void takeAllComments() throws DaoException {
	List<Comment> comments = commentDao.takeAll();
	assertThat(comments.size(), is(3));
    }

    /**
     * Take by news id.
     *
     * @throws DaoException
     *             the dao exception
     */
    @Test
    public void takeByNewsId() throws DaoException {
	List<Comment> comments = commentDao.takeByNewsId(1L);
	assertThat(comments.size(), is(1));
    }
}
