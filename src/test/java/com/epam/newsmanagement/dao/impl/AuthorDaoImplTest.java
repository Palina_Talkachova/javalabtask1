package com.epam.newsmanagement.dao.impl;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.*;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.List;

import org.dbunit.dataset.CompositeDataSet;
import org.dbunit.dataset.DataSetException;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.xml.FlatXmlDataSetBuilder;
import org.dbunit.operation.DatabaseOperation;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.epam.newsmanagement.config.ApplicationTestConfig;
import com.epam.newsmanagement.dao.AuthorDao;
import com.epam.newsmanagement.dao.exception.DaoException;
import com.epam.newsmanagement.entity.Author;

/**
 * The Class AuthorDAOImplementationTest.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = { ApplicationTestConfig.class })
public class AuthorDaoImplTest extends AbstractTest {
   
    @Autowired
    private AuthorDao authorDao;

    /**
     * takes data set to insert.
     *
     * @return the i data set
     * @throws DataSetException
     *             the data set exception
     * @throws FileNotFoundException
     *             the file not found exception
     * @see com.epam.newsmanagement.dao.impl.news.dao.implementation.AbstractTest#takeDataSetToInsert()
     */
    @Override
    public IDataSet takeDataSetToInsert() throws DataSetException, FileNotFoundException {
	IDataSet[] datasets = new IDataSet[] {
		new FlatXmlDataSetBuilder().build(new FileInputStream("src/test/resources/news_data.xml")),
		new FlatXmlDataSetBuilder().build(new FileInputStream("src/test/resources/author_data.xml")),
		new FlatXmlDataSetBuilder().build(new FileInputStream("src/test/resources/news_author_data.xml")) };
	return new CompositeDataSet(datasets);
    }

    /**
     * Configure db test case.
     *
     * @throws FileNotFoundException
     *             the file not found exception
     * @throws Exception
     *             the exception
     * @see com.epam.newsmanagement.dao.impl.news.dao.implementation.AbstractTest#configureDBTestCase()
     */
    @Override
    @Before
    public void configureDBTestCase() throws FileNotFoundException, Exception {
	DatabaseOperation.CLEAN_INSERT.execute(takeConnection(), takeDataSetToInsert());
    }

    /**
     * Adds an author.
     *
     * @throws DaoException
     *             the dao exception
     */
    @Test
    public void addAuthor() throws DaoException {
	Author author = new Author();
	author.setAuthorName("Viktor");
	long authorId = authorDao.add(author);
	assertNotEquals(0, authorId);
	assertNotNull(authorDao.takeById(authorId));
    }

    /**
     * Take by id.
     *
     * @throws DaoException
     *             the dao exception
     */
    @Test
    public void takeById() throws DaoException {
	Author author = new Author();
	author.setAuthorId(2L);
	author.setAuthorName("Kelly");
	assertEquals(author, authorDao.takeById(2L));
    }

    /**
     * Update author.
     *
     * @throws DaoException
     *             the dao exception
     */
    @Test
    public void updateAuthor() throws DaoException {
	Author author = authorDao.takeById(1L);
	author.setAuthorName("John");
	authorDao.update(author);
	String authorName = authorDao.takeById(1L).getAuthorName();
	assertEquals("John", authorName);
    }

    /**
     * Take all authors.
     *
     * @throws DaoException
     *             the dao exception
     */
    @Test
    public void takeAllAuthors() throws DaoException {
	List<Author> authors = authorDao.takeAll();
	assertThat(authors.size(), is(3));
    }

    /**
     * Expire author.
     *
     * @throws DaoException
     *             the dao exception
     */
    @Test
    public void expireAuthor() throws DaoException {
	Author author = authorDao.takeById(1L);
	authorDao.expireAuthor(author);
	assertNotNull(author.getExpired());
    }

    /**
     * Take by news id.
     *
     * @throws DaoException
     *             the dao exception
     */
    @Test
    public void takeByNewsId() throws DaoException {
	Author author = authorDao.takeByNewsId(1L);
	assertNotNull(author);
    }
}
