package com.epam.newsmanagement.dao.impl;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertThat;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.List;

import org.dbunit.dataset.CompositeDataSet;
import org.dbunit.dataset.DataSetException;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.xml.FlatXmlDataSetBuilder;
import org.dbunit.operation.DatabaseOperation;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.epam.newsmanagement.config.ApplicationTestConfig;
import com.epam.newsmanagement.dao.TagDao;
import com.epam.newsmanagement.dao.exception.DaoException;
import com.epam.newsmanagement.entity.Tag;

/**
 * The Class TagDaoTest.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = { ApplicationTestConfig.class })
public class TagDaoImplTest extends AbstractTest {
    @Autowired
    private TagDao tagDao;

    /**
     * Take data set to insert.
     *
     * @return the i data set
     * @throws DataSetException
     *             the data set exception
     * @throws FileNotFoundException
     *             the file not found exception
     * @see com.epam.newsmanagement.dao.impl.news.dao.implementation.AbstractTest#takeDataSetToInsert()
     */
    @Override
    public IDataSet takeDataSetToInsert() throws DataSetException, FileNotFoundException {
	IDataSet[] datasets = new IDataSet[] {
		new FlatXmlDataSetBuilder().build(new FileInputStream("src/test/resources/news_data.xml")),
		new FlatXmlDataSetBuilder().build(new FileInputStream("src/test/resources/tag_data.xml")),
		new FlatXmlDataSetBuilder().build(new FileInputStream("src/test/resources/news_tag_data.xml")) };
	return new CompositeDataSet(datasets);
    }

    /**
     * Configure db test case.
     *
     * @throws FileNotFoundException
     *             the file not found exception
     * @throws Exception
     *             the exception
     * @see com.epam.newsmanagement.dao.impl.news.dao.implementation.AbstractTest#configureDBTestCase()
     */
    @Override
    @Before
    public void configureDBTestCase() throws FileNotFoundException, Exception {
	DatabaseOperation.CLEAN_INSERT.execute(takeConnection(), takeDataSetToInsert());
    }

    /**
     * Adds the and delete tag.
     *
     * @throws DaoException
     *             the dao exception
     */
    @Test
    public void addTag() throws DaoException {
	Tag tag = new Tag();
	tag.setTagName("Name of a tag");
	long tagId = tagDao.add(tag);
	assertNotEquals(0, tagId);
	assertNotNull(tagDao.takeById(tagId));
    }
  
    /**
     * Deletes tag.
     *
     * @throws DaoException
     *             the dao exception
     */
    @Test
    public void deleteTag() throws DaoException {
	Tag tag = new Tag();
	tag.setTagId(4L);
	tag.setTagName("Name of a tag");
	long tagId = tagDao.add(tag);
	assertNotEquals(0, tagId);
	assertNotNull(tagDao.takeById(tagId));
	tagDao.deleteById(tagId);
	assertNull(tagDao.takeById(tagId));
    }
    
    /**
     * Take by id.
     *
     * @throws DaoException
     *             the dao exception
     */
    @Test
    public void takeById() throws DaoException {
	Tag tag = new Tag();
	tag.setTagId(1L);
	tag.setTagName("culture");
	assertEquals(tag, tagDao.takeById(1L));
    }

    /**
     * Update tag.
     *
     * @throws DaoException
     *             the dao exception
     */
    @Test
    public void updateTag() throws DaoException {
	Tag tag = tagDao.takeById(1L);
	tag.setTagName("music");
	tagDao.update(tag);
	assertEquals("music", tagDao.takeById(1L).getTagName());
    }

    /**
     * Take all tags.
     *
     * @throws DaoException
     *             the dao exception
     */
    @Test
    public void takeAllTags() throws DaoException {
	List<Tag> tags = tagDao.takeAll();
	assertThat(tags.size(), is(4));
    }

    /**
     * Take by news id.
     *
     * @throws DaoException
     *             the dao exception
     */
    @Test
    public void takeByNewsId() throws DaoException {
	List<Tag> tags = tagDao.takeByNewsId(1L);
	assertThat(tags.size(), is(1));
    }
}
