package com.epam.newsmanagement.config;

import javax.sql.DataSource;

import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

/**
 * 
 * Declares methods to provide and manage the data source for test DB.
 */
@Configuration
@PropertySource("classpath:db_test.properties")
public class ApplicationTestConfig extends BaseApplicationConfig {

    /**
     * 
     * @see com.epam.newsmanagement.config.BaseApplicationConfig#dataSource()
     */
    @Override
    public DataSource dataSource() {
	return super.dataSource();
    } 
}
