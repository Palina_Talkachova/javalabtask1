package com.epam.newsmanagement.dao;

import java.util.List;

import com.epam.newsmanagement.dao.exception.DaoException;

/**
 * The Interface IDao.
 * 
 * @author Palina_Talkachova
 * 
 *         involves CRUD and other methods to manage data of DB
 * 
 * @param <T>
 *            the generic type
 */
public interface Dao<T> {

    /**
     * adds an entity to DB.
     *
     * @param t
     * @throws DaoException
     */
    Long add(T t) throws DaoException;

    /**
     * gets an entity by id.
     *
     * @param id
     * @return an entity
     * @throws DaoException
     */
    T takeById(Long id) throws DaoException;

    /**
     * updates an entity.
     *
     * @param t
     * @throws DaoException
     */
    void update(T t) throws DaoException;

    /**
     * deletes an entity by id.
     *
     * @param id
     * @throws DaoException
     */
    void deleteById(Long id) throws DaoException;

    /**
     * gets all entities.
     *
     * @return a list with entities
     * @throws DaoException
     */
    List<T> takeAll() throws DaoException;
}
