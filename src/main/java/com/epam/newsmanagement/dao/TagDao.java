package com.epam.newsmanagement.dao;

import java.util.List;

import com.epam.newsmanagement.dao.exception.DaoException;
import com.epam.newsmanagement.entity.Tag;

/**
 * The Interface ITagDao.
 *
 * @author Palina_Talkachova
 */
public interface TagDao extends Dao<Tag> {

    /**
     * gets a list with tags by id of a news.
     *
     * @param newsId
     * @return a list with tags
     * @throws DaoException
     */
    List<Tag> takeByNewsId(Long newsId) throws DaoException;
}
