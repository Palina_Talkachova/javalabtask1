package com.epam.newsmanagement.dao;

import java.util.List;

import com.epam.newsmanagement.dao.exception.DaoException;
import com.epam.newsmanagement.entity.News;
import com.epam.newsmanagement.entity.SearchCriteria;

/**
 * The Interface INewsDao.
 *
 * @author Palina_Talkachova
 */
public interface NewsDao extends Dao<News> {

    /**
     * gets a list with news by id of an author.
     *
     * @param authorId
     * @return a list with news
     * @throws DaoException
     */
    List<News> takeByAuhtorId(Long authorId) throws DaoException;

    /**
     * gets a list with news by id of a tag.
     *
     * @param tagId
     * @return a list with news
     * @throws DaoException
     */
    List<News> takeByTagId(Long tagId) throws DaoException;

    /**
     * gets a list with news by a search criteria.
     *
     * @param searchCriteria
     * @return a list with news
     * @throws DaoException
     */
    List<News> findNewsBySearchCriteria(SearchCriteria searchCriteria) throws DaoException;

    /**
     * adds an author of a news by id of a news.
     *
     * @param the
     *            news id
     * @param the
     *            author id
     * @throws DaoException
     */
    void addNewsAuthor(Long newsId, Long authorId) throws DaoException;

    /**
     * adds a tag (tags) for a news
     * 
     * @param newsId
     * @param tagsId
     * @throws DaoException
     */
    void addNewsTagList(Long newsId, List<Long> tagsId) throws DaoException;

    /**
     * deletes news by a list of news id
     * 
     * @param newsIdList
     * @throws DaoException
     */
    void deleteNewsByNewsIdList(List<Long> newsIdList) throws DaoException;

    /**
     * deletes a news author by a news id
     * 
     * @param newsId
     * @throws DaoException
     */
    void deleteNewsAuthorByNewsId(Long newsId) throws DaoException;

    /**
     * updates a news author by a news id
     * 
     * @param newsId
     * @throws DaoException
     */
    void updateNewsAuthorByNewsId(Long authorId, Long newsId) throws DaoException;

    /**
     * deletes a news author by a list of news id
     * 
     * @param newsId
     * @throws DaoException
     */
    void deleteNewsAuthorByNewsIdList(List<Long> newsIdList) throws DaoException;

    /**
     * deletes a news tag by a news id
     * 
     * @param newsId
     * @throws DaoException
     */
    void deleteNewsTagByNewsId(Long newsId) throws DaoException;

    /**
     * deletes a list of tags by a news id
     * 
     * @param newsId
     * @throws DaoException
     */
    void deleteNewsTagByNewsIdList(List<Long> newsIdList) throws DaoException;

    /**
     * deletes comments by a list of news id
     * 
     * @param newsId
     * @throws DaoException
     */
    void deleteCommentsByNewsIdList(List<Long> newsIdList) throws DaoException;

    /**
     * counts all news.
     *
     * @return a quantity of news
     * @throws DaoException
     */
    Long countAllNews() throws DaoException;

    /**
     * finds and counts all news had been obtained by a search criteria
     *
     * @return a quantity of news
     * @throws DaoException
     */
    int findAndCountNewsBySearchCriteria(SearchCriteria searchCriteria) throws DaoException;
}
