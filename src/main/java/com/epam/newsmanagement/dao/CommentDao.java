package com.epam.newsmanagement.dao;

import java.util.List;

import com.epam.newsmanagement.dao.exception.DaoException;
import com.epam.newsmanagement.entity.Comment;

/**
 * The Interface ICommentDao.
 *
 * @author Palina_Talkachova
 */
public interface CommentDao extends Dao<Comment> {

    /**
     * gets a list of comments by id of a news.
     *
     * @param newsId
     * @return a list with comments
     * @throws DaoException
     */
    List<Comment> takeByNewsId(Long newsId) throws DaoException;
}
