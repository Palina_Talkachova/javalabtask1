package com.epam.newsmanagement.dao;

import com.epam.newsmanagement.dao.exception.DaoException;
import com.epam.newsmanagement.entity.Author;

/**
 * The Interface IAuthorDao.
 *
 * @author Palina_Talkachova
 */
public interface AuthorDao extends Dao<Author> {

    /**
     * expires an author.
     *
     * @param author
     * @throws DaoException
     *             the dao exception
     */
    void expireAuthor(Author author) throws DaoException;

    /**
     * gets an author by id of a news.
     *
     * @param newsId
     * @return an author
     * @throws DaoException
     *             the dao exception
     */
    Author takeByNewsId(Long newsId) throws DaoException;
}
