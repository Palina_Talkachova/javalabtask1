package com.epam.newsmanagement.dao.impl;

import static com.epam.newsmanagement.constant.FieldName.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.datasource.DataSourceUtils;
import org.springframework.stereotype.Repository;

import com.epam.newsmanagement.dao.CommentDao;
import com.epam.newsmanagement.dao.exception.DaoException;
import com.epam.newsmanagement.entity.Comment;

/**
 * The Class CommentDaoImpl.
 */
@Repository
public class CommentDaoImpl implements CommentDao {
    private static final String SQL_ADD_COMMENT = "INSERT INTO COMMENTS (NEWS_ID, COMMENT_TEXT, CREATION_DATE) VALUES (?,?,?)";
    private static final String SQL_GET_COMMENT_BY_ID = "SELECT COMMENT_ID, NEWS_ID, COMMENT_TEXT, CREATION_DATE FROM COMMENTS WHERE COMMENT_ID = ?";
    private static final String SQL_UPDATE_COMMENT = "UPDATE COMMENTS SET NEWS_ID = ?, COMMENT_TEXT = ?, CREATION_DATE = ? WHERE COMMENT_ID = ?";
    private static final String SQL_DELETE_COMMENT = "DELETE FROM COMMENTS WHERE COMMENT_ID = ?";
    private static final String SQL_GET_ALL_COMMENTS = "SELECT COMMENT_ID, NEWS_ID, COMMENT_TEXT, CREATION_DATE FROM COMMENTS";
    private static final String SQL_GET_COMMENTS_BY_NEWS_ID = "SELECT COMMENT_ID, NEWS_ID, COMMENT_TEXT, CREATION_DATE FROM COMMENTS WHERE NEWS_ID = ?";

    @Autowired
    private DataSource dataSource;

    /**
     * 
     * @see com.epam.newsmanagement.dao.CommentDao#add(com.epam.newsmanagement.entity
     *      .Comment)
     */
    @Override
    public Long add(Comment comment) throws DaoException {
	Connection connection = DataSourceUtils.getConnection(dataSource);
	String[] columnNames = { COMMENT_ID };
	Long commentId = null;
	try (PreparedStatement preparedStatement = connection.prepareStatement(SQL_ADD_COMMENT, columnNames)) {
	    preparedStatement.setLong(1, comment.getNewsId());
	    preparedStatement.setString(2, comment.getCommentText());
	    Timestamp timestamp = new Timestamp(System.currentTimeMillis());
	    preparedStatement.setTimestamp(3, timestamp);
	    preparedStatement.executeUpdate();
	    try (ResultSet resultSet = preparedStatement.getGeneratedKeys()) {
		if (resultSet.next()) {
		    commentId = resultSet.getLong(1);
		}
	    }
	} catch (SQLException e) {
	    throw new DaoException("SQLException occurred while a comment was being saved to the database", e);
	} finally {
	    DataSourceUtils.releaseConnection(connection, dataSource);
	}
	return commentId;
    }

    /**
     * 
     * @see com.epam.newsmanagement.dao.CommentDao#takeById(java.lang.Long)
     */
    @Override
    public Comment takeById(Long commentId) throws DaoException {
	Connection connection = DataSourceUtils.getConnection(dataSource);
	Comment comment = null;
	try (PreparedStatement preparedStatement = connection.prepareStatement(SQL_GET_COMMENT_BY_ID)) {
	    preparedStatement.setLong(1, commentId);
	    ResultSet resultSet = preparedStatement.executeQuery();
	    if (resultSet.next()) {
		comment = parseResultSet(resultSet);
	    }
	} catch (SQLException e) {
	    throw new DaoException("SQLException occurred while a comment was being got by id from the database", e);
	} finally {
	    DataSourceUtils.releaseConnection(connection, dataSource);
	}
	return comment;
    }

    /**
     * 
     * @see com.epam.newsmanagement.dao.CommentDao#update(com.epam.newsmanagement.
     *      entity.Comment)
     */
    @Override
    public void update(Comment comment) throws DaoException {
	Connection connection = DataSourceUtils.getConnection(dataSource);
	try (PreparedStatement preparedStatement = connection.prepareStatement(SQL_UPDATE_COMMENT)) {
	    preparedStatement.setLong(1, comment.getNewsId());
	    preparedStatement.setString(2, comment.getCommentText());
	    Timestamp timestamp = new Timestamp(System.currentTimeMillis());
	    preparedStatement.setTimestamp(3, timestamp);
	    preparedStatement.setLong(4, comment.getCommentId());
	    preparedStatement.executeUpdate();
	} catch (SQLException e) {
	    throw new DaoException("SQLException occurred while a comment was being updated in the database", e);
	} finally {
	    DataSourceUtils.releaseConnection(connection, dataSource);
	}
    }

    /**
     * 
     * @see com.epam.newsmanagement.dao.CommentDao#deleteById(java.lang.Long)
     */
    @Override
    public void deleteById(Long commentId) throws DaoException {
	Connection connection = DataSourceUtils.getConnection(dataSource);
	try (PreparedStatement preparedStatement = connection.prepareStatement(SQL_DELETE_COMMENT)) {
	    preparedStatement.setLong(1, commentId);
	    preparedStatement.executeUpdate();
	} catch (SQLException e) {
	    throw new DaoException("SQLException occurred while a comment was being deleted from the database", e);
	} finally {
	    DataSourceUtils.releaseConnection(connection, dataSource);
	}
    }

    /**
     * 
     * @see com.epam.newsmanagement.dao.CommentDao#takeAll()
     */
    @Override
    public List<Comment> takeAll() throws DaoException {
	Connection connection = DataSourceUtils.getConnection(dataSource);
	List<Comment> comments = new ArrayList<>();
	try (Statement statement = connection.createStatement()) {
	    ResultSet resultSet = statement.executeQuery(SQL_GET_ALL_COMMENTS);
	    while (resultSet.next()) {
		comments.add(parseResultSet(resultSet));
	    }
	} catch (SQLException e) {
	    throw new DaoException("SQLException occurred while all comments was being got from the database", e);
	} finally {
	    DataSourceUtils.releaseConnection(connection, dataSource);
	}
	return comments;
    }

    /**
     * 
     * @see com.epam.newsmanagement.dao.CommentDao#takeByNewsId(java.lang.Long)
     */
    @Override
    public List<Comment> takeByNewsId(Long newsId) throws DaoException {
	Connection connection = DataSourceUtils.getConnection(dataSource);
	List<Comment> comments = new ArrayList<>();
	try (PreparedStatement preparedStatement = connection.prepareStatement(SQL_GET_COMMENTS_BY_NEWS_ID)) {
	    preparedStatement.setLong(1, newsId);
	    ResultSet resultSet = preparedStatement.executeQuery();
	    while (resultSet.next()) {
		comments.add(parseResultSet(resultSet));
	    }
	} catch (SQLException e) {
	    throw new DaoException("SQLException occurred while comments was being got by news id from the database",
		    e);
	} finally {
	    DataSourceUtils.releaseConnection(connection, dataSource);
	}
	return comments;
    }

    /**
     * parses a set with dates about comments.
     *
     * @param resultSet
     * @return a list with comments
     * @throws DaoException
     * 
     */
    private Comment parseResultSet(ResultSet resultSet) throws DaoException {
	Comment comment = new Comment();
	try {
	    comment.setCommentId(resultSet.getLong(COMMENT_ID));
	    comment.setNewsId(resultSet.getLong(NEWS_ID));
	    comment.setCommentText(resultSet.getString(COMMENT_TEXT));
	    comment.setCreationDate(resultSet.getTimestamp(COMMENT_CREATION_DATE));
	} catch (SQLException e) {
	    throw new DaoException("SQLException occurred while was being parsed a result set for comments", e);
	}
	return comment;
    }
}
