package com.epam.newsmanagement.dao.impl;

import static com.epam.newsmanagement.constant.FieldName.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.datasource.DataSourceUtils;
import org.springframework.stereotype.Repository;

import com.epam.newsmanagement.dao.AuthorDao;
import com.epam.newsmanagement.dao.exception.DaoException;
import com.epam.newsmanagement.entity.Author;

/**
 * The Class AuthorDaoImpl.
 */
@Repository
public class AuthorDaoImpl implements AuthorDao {
    private static final String SQL_ADD_AUTHOR = "INSERT INTO AUTHOR (AUTHOR_NAME) VALUES (?)";
    private static final String SQL_AUTHOR_BY_ID = "SELECT AUTHOR_ID, AUTHOR_NAME, EXPIRED FROM AUTHOR WHERE AUTHOR_ID = ?";
    private static final String SQL_UPDATE_AUTHOR = "UPDATE AUTHOR SET AUTHOR_NAME = ?, EXPIRED = ? WHERE AUTHOR_ID = ?";
    private static final String SQL_GET_ALL_AUTHORS = "SELECT AUTHOR_ID, AUTHOR_NAME, EXPIRED FROM AUTHOR";
    private static final String SQL_SET_EXPIRED = "UPDATE AUTHOR SET EXPIRED = ? WHERE AUTHOR_ID = ?";
    private static final String SQL_GET_AUTHOR_BY_NEWS_ID = "SELECT AUTHOR.AUTHOR_ID, AUTHOR_NAME, EXPIRED FROM NEWS_AUTHOR JOIN AUTHOR ON (NEWS_AUTHOR.AUTHOR_ID = AUTHOR.AUTHOR_ID) WHERE NEWS_ID = ?";

    @Autowired
    private DataSource dataSource;

    /**
     * 
     * @see com.epam.newsmanagement.dao.AuthorDao#add(com.epam.newsmanagement.entity
     *      .Author)
     */
    @Override
    public Long add(Author author) throws DaoException {
	Connection connection = DataSourceUtils.getConnection(dataSource);
	String[] columnNames = { AUTHOR_ID };
	Long authorId = null;
	try (PreparedStatement preparedStatement = connection.prepareStatement(SQL_ADD_AUTHOR, columnNames)) {
	    preparedStatement.setString(1, author.getAuthorName());
	    preparedStatement.executeUpdate();
	    try (ResultSet resultSet = preparedStatement.getGeneratedKeys()) {
		if (resultSet.next()) {
		    authorId = resultSet.getLong(1);
		}
	    }
	} catch (SQLException e) {
	    throw new DaoException("SQLException occurred while an auhtor was being saved to the database", e);
	} finally {
	    DataSourceUtils.releaseConnection(connection, dataSource);
	}
	return authorId;
    }

    /**
     * 
     * @see com.epam.newsmanagement.dao.AuthorDao#takeById(java.lang.Long)
     */
    @Override
    public Author takeById(Long authorId) throws DaoException {
	Connection connection = DataSourceUtils.getConnection(dataSource);
	Author author = null;
	try (PreparedStatement preparedStatement = connection.prepareStatement(SQL_AUTHOR_BY_ID)) {
	    preparedStatement.setLong(1, authorId);
	    ResultSet resultSet = preparedStatement.executeQuery();
	    if (resultSet.next()) {
		author = parseResultSet(resultSet);
	    }
	} catch (SQLException e) {
	    throw new DaoException("SQLException occurred while an auhtor was being got by id from the database", e);
	} finally {
	    DataSourceUtils.releaseConnection(connection, dataSource);
	}
	return author;
    }

    /**
     * 
     * @see com.epam.newsmanagement.dao.AuthorDao#update(com.epam.newsmanagement.
     *      entity.Author)
     */
    @Override
    public void update(Author author) throws DaoException {
	Connection connection = DataSourceUtils.getConnection(dataSource);
	try (PreparedStatement preparedStatement = connection.prepareStatement(SQL_UPDATE_AUTHOR)) {
	    preparedStatement.setString(1, author.getAuthorName());
	    preparedStatement.setTimestamp(2,
		    author.getExpired() != null ? new Timestamp(author.getExpired().getTime()) : null);
	    preparedStatement.setLong(3, author.getAuthorId());
	    preparedStatement.executeUpdate();
	} catch (SQLException e) {
	    throw new DaoException("SQLException occurred while an auhtor was being updated in the database", e);
	} finally {
	    DataSourceUtils.releaseConnection(connection, dataSource);
	}
    }

    @Override
    public void deleteById(Long id) {
	throw new UnsupportedOperationException("The method to delete an author by id is in developing");
    }

    /**
     * 
     * @see com.epam.newsmanagement.dao.AuthorDao#takeAll()
     */
    @Override
    public List<Author> takeAll() throws DaoException {
	Connection connection = DataSourceUtils.getConnection(dataSource);
	List<Author> authors = new ArrayList<>();
	try (Statement statement = connection.createStatement()) {
	    ResultSet resultSet = statement.executeQuery(SQL_GET_ALL_AUTHORS);
	    while (resultSet.next()) {
		authors.add(parseResultSet(resultSet));
	    }
	} catch (SQLException e) {
	    throw new DaoException("SQLException occurred while all auhtors was being got from the database", e);
	} finally {
	    DataSourceUtils.releaseConnection(connection, dataSource);
	}
	return authors;
    }

    /**
     * 
     * @see com.epam.newsmanagement.dao.AuthorDao#expireAuthor(com.epam.
     *      newsmanagement.entity.Author)
     */
    @Override
    public void expireAuthor(Author author) throws DaoException {
	Connection connection = DataSourceUtils.getConnection(dataSource);
	try (PreparedStatement preparedStatement = connection.prepareStatement(SQL_SET_EXPIRED)) {
	    Timestamp timestamp = new Timestamp(System.currentTimeMillis());
	    preparedStatement.setTimestamp(1, timestamp);
	    preparedStatement.setLong(2, author.getAuthorId());
	    preparedStatement.executeUpdate();
	    author.setExpired(timestamp);
	} catch (SQLException e) {
	    throw new DaoException("SQLException occurred while an auhtor was being expired in the database", e);
	} finally {
	    DataSourceUtils.releaseConnection(connection, dataSource);
	}
    }

    /**
     * 
     * @see com.epam.newsmanagement.dao.AuthorDao#takeByNewsId(java.lang.Long)
     */
    @Override
    public Author takeByNewsId(Long newsId) throws DaoException {
	Connection connection = DataSourceUtils.getConnection(dataSource);
	Author author = null;
	try (PreparedStatement preparedStatement = connection.prepareStatement(SQL_GET_AUTHOR_BY_NEWS_ID)) {
	    preparedStatement.setLong(1, newsId);
	    ResultSet resultSet = preparedStatement.executeQuery();
	    if (resultSet.next()) {
		author = parseResultSet(resultSet);
	    }
	} catch (SQLException e) {
	    throw new DaoException("SQLException occurred while an auhtor was being got by news id from the database",
		    e);
	} finally {
	    DataSourceUtils.releaseConnection(connection, dataSource);
	}
	return author;
    }

    /**
     * parses a set with author's dates.
     * 
     * @param resultSet
     * @return author object
     * @throws DaoException
     */
    private Author parseResultSet(ResultSet resultSet) throws DaoException {
	Author author = new Author();
	try {
	    author.setAuthorId(resultSet.getLong(AUTHOR_ID));
	    author.setAuthorName(resultSet.getString(AUTHOR_NAME));
	    author.setExpired(resultSet.getTimestamp(AUTHOR_EXPIRED));
	} catch (SQLException e) {
	    throw new DaoException("SQLException occurred while was being parsed a result set for authors", e);
	}
	return author;
    }
}
