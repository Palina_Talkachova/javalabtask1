package com.epam.newsmanagement.dao.impl;

import static com.epam.newsmanagement.constant.FieldName.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.datasource.DataSourceUtils;
import org.springframework.stereotype.Repository;

import com.epam.newsmanagement.dao.TagDao;
import com.epam.newsmanagement.dao.exception.DaoException;
import com.epam.newsmanagement.entity.Tag;

/**
 * The Class TagDaoImpl.
 */
@Repository
public class TagDaoImpl implements TagDao {
    private static final String SQL_ADD_TAG = "INSERT INTO TAG (TAG_NAME) VALUES (?)";
    private static final String SQL_GET_TAG_BY_ID = "SELECT TAG_ID, TAG_NAME FROM TAG WHERE TAG_ID = ?";
    private static final String SQL_UPDATE_TAG = "UPDATE TAG SET TAG_NAME = ? WHERE TAG_ID = ?";
    private static final String SQL_DELETE_TAG = "DELETE FROM TAG WHERE TAG_ID = ?";
    private static final String SQL_GET_ALL_TAGS = "SELECT TAG_ID, TAG_NAME FROM TAG";
    private static final String SQL_GET_TAGS_BY_NEWS_ID = "SELECT TAG.TAG_ID, TAG_NAME FROM TAG JOIN NEWS_TAG ON (TAG.TAG_ID = NEWS_TAG.TAG_ID) WHERE NEWS_ID = ?";
    @Autowired
    private DataSource dataSource;

    /**
     * 
     * @see com.epam.newsmanagement.dao.TagDao#add(com.epam.newsmanagement.entity.
     *      Tag)
     */
    @Override
    public Long add(Tag tag) throws DaoException {
	Connection connection = DataSourceUtils.getConnection(dataSource);
	String[] columnNames = { TAG_ID };
	Long tagId = null;
	try (PreparedStatement preparedStatement = connection.prepareStatement(SQL_ADD_TAG, columnNames)) {
	    preparedStatement.setString(1, tag.getTagName());
	    preparedStatement.executeUpdate();
	    try (ResultSet resultSet = preparedStatement.getGeneratedKeys()) {
		if (resultSet.next()) {
		    tagId = resultSet.getLong(1);
		}
	    }
	} catch (SQLException e) {
	    throw new DaoException("SQLException occurred while a tag was being saved to the database", e);
	} finally {
	    DataSourceUtils.releaseConnection(connection, dataSource);
	}
	return tagId;
    }

    /**
     * 
     * @see com.epam.newsmanagement.dao.TagDao#takeById(java.lang.Long)
     */
    @Override
    public Tag takeById(Long tagId) throws DaoException {
	Connection connection = DataSourceUtils.getConnection(dataSource);
	Tag tag = null;
	try (PreparedStatement preparedStatement = connection.prepareStatement(SQL_GET_TAG_BY_ID)) {
	    preparedStatement.setLong(1, tagId);
	    ResultSet resultSet = preparedStatement.executeQuery();
	    if (resultSet.next()) {
		tag = parseResultSet(resultSet);
	    }
	} catch (SQLException e) {
	    throw new DaoException("SQLException occurred while a tag was being updated in the database", e);
	} finally {
	    DataSourceUtils.releaseConnection(connection, dataSource);
	}
	return tag;
    }

    /**
     * 
     * @see com.epam.newsmanagement.dao.TagDao#update(com.epam.newsmanagement.entity
     *      .Tag)
     */
    @Override
    public void update(Tag tag) throws DaoException {
	Connection connection = DataSourceUtils.getConnection(dataSource);
	try (PreparedStatement preparedStatement = connection.prepareStatement(SQL_UPDATE_TAG)) {
	    preparedStatement.setString(1, tag.getTagName());
	    preparedStatement.setLong(2, tag.getTagId());
	    preparedStatement.executeUpdate();
	} catch (SQLException e) {
	    throw new DaoException("SQLException occurred while a tag was being updated in the database", e);
	} finally {
	    DataSourceUtils.releaseConnection(connection, dataSource);
	}
    }

    /**
     * 
     * @see com.epam.newsmanagement.dao.TagDao#deleteById(java.lang.Long)
     */
    @Override
    public void deleteById(Long tagId) throws DaoException {
	Connection connection = DataSourceUtils.getConnection(dataSource);
	try (PreparedStatement preparedStatement = connection.prepareStatement(SQL_DELETE_TAG)) {
	    preparedStatement.setLong(1, tagId);
	    preparedStatement.executeUpdate();
	} catch (SQLException e) {
	    throw new DaoException("SQLException occurred while a tag was being deleted from the database", e);
	} finally {
	    DataSourceUtils.releaseConnection(connection, dataSource);
	}
    }

    /**
     * 
     * @see com.epam.newsmanagement.dao.TagDao#takeAll()
     */
    @Override
    public List<Tag> takeAll() throws DaoException {
	Connection connection = DataSourceUtils.getConnection(dataSource);
	List<Tag> tags = new ArrayList<>();
	try (Statement statement = connection.createStatement()) {
	    ResultSet resultSet = statement.executeQuery(SQL_GET_ALL_TAGS);
	    while (resultSet.next()) {
		tags.add(parseResultSet(resultSet));
	    }
	} catch (SQLException e) {
	    throw new DaoException("SQLException occurred while all tags was being got from the database", e);
	} finally {
	    DataSourceUtils.releaseConnection(connection, dataSource);
	}
	return tags;
    }

    /**
     * 
     * @see com.epam.newsmanagement.dao.TagDao#takeByNewsId(java.lang.Long)
     */
    @Override
    public List<Tag> takeByNewsId(Long newsId) throws DaoException {
	Connection connection = DataSourceUtils.getConnection(dataSource);
	List<Tag> tags = new ArrayList<>();
	try (PreparedStatement preparedStatement = connection.prepareStatement(SQL_GET_TAGS_BY_NEWS_ID)) {
	    preparedStatement.setLong(1, newsId);
	    ResultSet resultSet = preparedStatement.executeQuery();
	    while (resultSet.next()) {
		tags.add(parseResultSet(resultSet));
	    }
	} catch (SQLException e) {
	    throw new DaoException("SQLException occurred while tags was being got by a news id from the database", e);
	} finally {
	    DataSourceUtils.releaseConnection(connection, dataSource);
	}
	return tags;
    }

    /**
     * parses a set with dates about tag.
     *
     * @param resultSet
     * @return a list with tags
     * @throws DaoException
     * 
     */
    private Tag parseResultSet(ResultSet resultSet) throws DaoException {
	Tag tag = new Tag();
	try {
	    tag.setTagId(resultSet.getLong(TAG_ID));
	    tag.setTagName(resultSet.getString(TAG_NAME));
	} catch (SQLException e) {
	    throw new DaoException("SQLException occurred while was being parsed a result set for tags", e);
	}
	return tag;
    }
}
