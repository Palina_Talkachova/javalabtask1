package com.epam.newsmanagement.dao.impl;

import static com.epam.newsmanagement.constant.FieldName.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.datasource.DataSourceUtils;
import org.springframework.stereotype.Repository;

import com.epam.newsmanagement.dao.NewsDao;
import com.epam.newsmanagement.dao.exception.DaoException;
import com.epam.newsmanagement.entity.News;
import com.epam.newsmanagement.entity.SearchCriteria;

/**
 * The Class NewsDaoImpl.
 */
@Repository
public class NewsDaoImpl implements NewsDao {
    private static final String SQL_ADD_NEWS = "INSERT INTO NEWS (TITLE, SHORT_TEXT, FULL_TEXT, CREATION_DATE, MODIFICATION_DATE) VALUES (?,?,?,?,?)";
    private static final String SQL_GET_NEWS_BY_ID = "SELECT NEWS_ID, TITLE, SHORT_TEXT, FULL_TEXT, CREATION_DATE, MODIFICATION_DATE FROM NEWS WHERE NEWS_ID = ?";
    private static final String SQL_UPDATE_NEWS = "UPDATE NEWS SET TITLE = ?, SHORT_TEXT = ?, FULL_TEXT = ?, CREATION_DATE = ?, MODIFICATION_DATE = ? WHERE NEWS_ID = ?";
    private static final String SQL_DELETE_NEWS = "DELETE FROM NEWS WHERE NEWS_ID = ?";
    private static final String SQL_DELETE_NEWS_BY_NEWS_ID_LIST = "DELETE FROM NEWS WHERE NEWS_ID IN (";
    private static final String SQL_GET_ALL_NEWS = "SELECT NEWS.NEWS_ID, NEWS.TITLE, NEWS.SHORT_TEXT, NEWS.FULL_TEXT, NEWS.CREATION_DATE, NEWS.MODIFICATION_DATE FROM NEWS LEFT JOIN COMMENTS ON (NEWS.NEWS_ID = COMMENTS.NEWS_ID) GROUP BY NEWS.NEWS_ID, NEWS.TITLE, NEWS.SHORT_TEXT, NEWS.FULL_TEXT, NEWS.CREATION_DATE, NEWS.MODIFICATION_DATE ORDER BY COUNT(COMMENT_ID) DESC, NEWS.MODIFICATION_DATE DESC";
    private static final String SQL_GET_NEWS_BY_AUTHOR_ID = "SELECT NEWS.NEWS_ID, TITLE, SHORT_TEXT, FULL_TEXT, NEWS.CREATION_DATE, MODIFICATION_DATE FROM NEWS JOIN NEWS_AUTHOR ON (NEWS.NEWS_ID = NEWS_AUTHOR.NEWS_ID) LEFT JOIN COMMENTS ON (NEWS.NEWS_ID = COMMENTS.NEWS_ID) WHERE AUTHOR_ID = ? GROUP BY NEWS.NEWS_ID, TITLE, SHORT_TEXT, FULL_TEXT, NEWS.CREATION_DATE, MODIFICATION_DATE ORDER BY COUNT(COMMENT_ID) DESC, NEWS.MODIFICATION_DATE DESC";
    private static final String SQL_GET_NEWS_BY_TAG_ID = "SELECT NEWS.NEWS_ID, TITLE, SHORT_TEXT, FULL_TEXT, NEWS.CREATION_DATE, MODIFICATION_DATE FROM NEWS JOIN NEWS_TAG ON (NEWS.NEWS_ID = NEWS_TAG.NEWS_ID) LEFT JOIN COMMENTS ON (NEWS.NEWS_ID = COMMENTS.NEWS_ID) WHERE TAG_ID = ? GROUP BY NEWS.NEWS_ID, TITLE, SHORT_TEXT, FULL_TEXT, NEWS.CREATION_DATE, MODIFICATION_DATE ORDER BY COUNT(COMMENT_ID) DESC, NEWS.MODIFICATION_DATE DESC";
    private static final String SQL_ADD_NEWS_AUTHOR = "INSERT INTO NEWS_AUTHOR (NEWS_ID, AUTHOR_ID) VALUES (?,?)";
    private static final String SQL_ADD_NEWS_TAG = "INSERT INTO NEWS_TAG (NEWS_ID, TAG_ID) VALUES (?,?)";
    private static final String SQL_SEARCH_NEWS = "SELECT DISTINCT NEWS.NEWS_ID, TITLE, SHORT_TEXT, FULL_TEXT, NEWS.CREATION_DATE, MODIFICATION_DATE FROM NEWS";
    private static final String SQL_COMPLEX_SEARCH_NEWS = " JOIN NEWS_AUTHOR ON (NEWS.NEWS_ID = NEWS_AUTHOR.NEWS_ID) JOIN NEWS_TAG ON (NEWS.NEWS_ID = NEWS_TAG.NEWS_ID) LEFT JOIN COMMENTS ON (NEWS.NEWS_ID = COMMENTS.NEWS_ID) WHERE  AUTHOR_ID = ";
    private static final String SQL_COMPLEX_SEARCH_NEWS_AUTHOR = " JOIN NEWS_AUTHOR ON (NEWS.NEWS_ID = NEWS_AUTHOR.NEWS_ID) LEFT JOIN COMMENTS ON (NEWS.NEWS_ID = COMMENTS.NEWS_ID) WHERE AUTHOR_ID = ";
    private static final String SQL_COMPLEX_SEARCH_NEWS_TAG = " JOIN NEWS_TAG ON (NEWS.NEWS_ID = NEWS_TAG.NEWS_ID) LEFT JOIN COMMENTS ON (NEWS.NEWS_ID = COMMENTS.NEWS_ID) WHERE";
    private static final String SQL_DELETE_NEWS_AUTHOR_BY_NEWS_ID = "DELETE FROM NEWS_AUTHOR WHERE NEWS_ID = ?";
    private static final String SQL_UPDATE_NEWS_AUTHOR_BY_NEWS_ID = "UPDATE NEWS_AUTHOR SET AUTHOR_ID = ? WHERE NEWS_ID = ?";
    private static final String SQL_DELETE_NEWS_AUTHOR_BY_NEWS_ID_LIST = "DELETE FROM NEWS_AUTHOR WHERE NEWS_ID IN (";
    private static final String SQL_DELETE_NEWS_TAG = "DELETE FROM NEWS_TAG WHERE NEWS_ID = ?";
    private static final String SQL_DELETE_NEWS_TAG_BY_NEWS_ID_LIST = "DELETE FROM NEWS_TAG WHERE NEWS_ID IN (";
    private static final String SQL_DELETE_COMMENTS_BY_NEWS_ID_LIST = "DELETE FROM COMMENTS WHERE NEWS_ID IN (";
    private static final String SQL_COUNT_ALL_NEWS = "SELECT COUNT(*) FROM NEWS";

    @Autowired
    private DataSource dataSource;

    /**
     * 
     * @see com.epam.newsmanagement.dao.NewsDao#add(com.epam.newsmanagement.entity.
     *      News)
     */
    @Override
    public Long add(News news) throws DaoException {
	Connection connection = DataSourceUtils.getConnection(dataSource);
	String[] columnNames = { NEWS_ID };
	Long newsId = null;
	try (PreparedStatement preparedStatement = connection.prepareStatement(SQL_ADD_NEWS, columnNames)) {
	    preparedStatement.setString(1, news.getTitle());
	    preparedStatement.setString(2, news.getShortText());
	    preparedStatement.setString(3, news.getFullText());
	    preparedStatement.setTimestamp(4, new Timestamp(news.getCreationDate().getTime()));
	    preparedStatement.setDate(5, news.getModificationDate());
	    preparedStatement.executeUpdate();
	    try (ResultSet resultSet = preparedStatement.getGeneratedKeys()) {
		if (resultSet.next()) {
		    newsId = resultSet.getLong(1);
		}
	    }
	} catch (SQLException e) {
	    throw new DaoException("SQLException occurred while a news was being saved to the database", e);
	} finally {
	    DataSourceUtils.releaseConnection(connection, dataSource);
	}
	return newsId;
    }

    /**
     * 
     * @see com.epam.newsmanagement.dao.NewsDao#takeById(java.lang.Long)
     */
    @Override
    public News takeById(Long newsId) throws DaoException {
	News news = null;
	Connection connection = DataSourceUtils.getConnection(dataSource);
	try (PreparedStatement preparedStatement = connection.prepareStatement(SQL_GET_NEWS_BY_ID)) {
	    preparedStatement.setLong(1, newsId);
	    ResultSet resultSet = preparedStatement.executeQuery();
	    if (resultSet.next()) {
		news = parseResultSet(resultSet);
	    }
	} catch (SQLException e) {
	    throw new DaoException("SQLException occurred while a news was being got by id from the database", e);
	} finally {
	    DataSourceUtils.releaseConnection(connection, dataSource);
	}
	return news;
    }

    /**
     * 
     * @see com.epam.newsmanagement.dao.NewsDao#update(com.epam.newsmanagement.
     *      entity.News)
     */
    @Override
    public void update(News news) throws DaoException {
	Connection connection = DataSourceUtils.getConnection(dataSource);
	try (PreparedStatement preparedStatement = connection.prepareStatement(SQL_UPDATE_NEWS)) {
	    preparedStatement.setString(1, news.getTitle());
	    preparedStatement.setString(2, news.getShortText());
	    preparedStatement.setString(3, news.getFullText());
	    preparedStatement.setTimestamp(4, new Timestamp(news.getCreationDate().getTime()));
	    preparedStatement.setDate(5, news.getModificationDate());
	    preparedStatement.setLong(6, news.getNewsId());
	    preparedStatement.executeUpdate();
	} catch (SQLException e) {
	    throw new DaoException("SQLException occurred while a news was being updated in the database", e);
	} finally {
	    DataSourceUtils.releaseConnection(connection, dataSource);
	}
    }

    /**
     * 
     * @see com.epam.newsmanagement.dao.NewsDao#deleteById(java.lang.Long)
     */
    @Override
    public void deleteById(Long newsId) throws DaoException {
	Connection connection = DataSourceUtils.getConnection(dataSource);
	try (PreparedStatement preparedStatement = connection.prepareStatement(SQL_DELETE_NEWS)) {
	    preparedStatement.setLong(1, newsId);
	    preparedStatement.executeUpdate();
	} catch (SQLException e) {
	    throw new DaoException("SQLException occurred while a news was being deleted from the database", e);
	} finally {
	    DataSourceUtils.releaseConnection(connection, dataSource);
	}
    }

    /**
     * 
     * @see com.epam.newsmanagement.dao.NewsDao#deleteNewsByNewsIdList(java.lang.Long)
     */
    public void deleteNewsByNewsIdList(List<Long> newsIdList) throws DaoException {
	Connection connection = DataSourceUtils.getConnection(dataSource);
	StringBuilder query = new StringBuilder(SQL_DELETE_NEWS_BY_NEWS_ID_LIST);
	//TODO 
	Iterator<Long> iterator = newsIdList.iterator();
	while (iterator.hasNext()) {
	    query.append(iterator.next());
	    if (iterator.hasNext()) {
		query.append(", ");
	    } else {
		query.append(")");
	    }
	}
	try (Statement statement = connection.createStatement()) {
	    statement.executeQuery(query.toString());
	} catch (SQLException e) {
	    throw new DaoException(
		    "SQLException occurred while news was being deleted by a list of news id from the database", e);
	} finally {
	    DataSourceUtils.releaseConnection(connection, dataSource);
	}
    }

    /**
     * gets all news sorted by quantity of comments and modification date
     * 
     */
    @Override
    public List<News> takeAll() throws DaoException {
	Connection connection = DataSourceUtils.getConnection(dataSource);	
	List<News> newsList = new ArrayList<>();
	try (Statement statement = connection.createStatement()) {
	    ResultSet resultSet = statement.executeQuery(SQL_GET_ALL_NEWS);
	    while (resultSet.next()) {
		newsList.add(parseResultSet(resultSet));
	    }
	} catch (SQLException e) {
	    throw new DaoException("SQLException occurred while all news was being got from the database", e);
	} finally {
	    DataSourceUtils.releaseConnection(connection, dataSource);
	}
	return newsList;
    }

    /**
     * 
     * @see com.epam.newsmanagement.dao.NewsDao#takeByAuhtorId(java.lang.Long)
     */
    @Override
    public List<News> takeByAuhtorId(Long authorId) throws DaoException {
	Connection connection = DataSourceUtils.getConnection(dataSource);
	List<News> newsList = new ArrayList<>();
	try (PreparedStatement preparedStatement = connection.prepareStatement(SQL_GET_NEWS_BY_AUTHOR_ID)) {
	    preparedStatement.setLong(1, authorId);
	    ResultSet resultSet = preparedStatement.executeQuery();
	    while (resultSet.next()) {
		newsList.add(parseResultSet(resultSet));
	    }
	} catch (SQLException e) {
	    throw new DaoException("SQLException occurred while news was being got by an author's id from the database",
		    e);
	} finally {
	    DataSourceUtils.releaseConnection(connection, dataSource);
	}
	return newsList;
    }

    /**
     * 
     * @see com.epam.newsmanagement.dao.NewsDao#takeByTagId(java.lang.Long)
     */
    @Override
    public List<News> takeByTagId(Long tagId) throws DaoException {
	Connection connection = DataSourceUtils.getConnection(dataSource);
	List<News> newsList = new ArrayList<>();
	try (PreparedStatement preparedStatement = connection.prepareStatement(SQL_GET_NEWS_BY_TAG_ID)) {
	    preparedStatement.setLong(1, tagId);
	    ResultSet resultSet = preparedStatement.executeQuery();
	    while (resultSet.next()) {
		newsList.add(parseResultSet(resultSet));
	    }
	} catch (SQLException e) {
	    throw new DaoException("SQLException occurred while news was being got by tag id from the database", e);
	} finally {
	    DataSourceUtils.releaseConnection(connection, dataSource);
	}
	return newsList;
    }

    /**
     * 
     * @see com.epam.newsmanagement.dao.NewsDao#addNewsAuthor(java.lang.Long,
     *      java.lang.Long)
     */
    @Override
    public void addNewsAuthor(Long newsId, Long authorId) throws DaoException {
	Connection connection = DataSourceUtils.getConnection(dataSource);
	try (PreparedStatement preparedStatement = connection.prepareStatement(SQL_ADD_NEWS_AUTHOR)) {
	    preparedStatement.setLong(1, newsId);
	    preparedStatement.setLong(2, authorId);
	    preparedStatement.executeUpdate();
	} catch (SQLException e) {
	    throw new DaoException("SQLException occurred while a news author was being saved to the database", e);
	} finally {
	    DataSourceUtils.releaseConnection(connection, dataSource);
	}
    }

    /**
     * 
     * @see com.epam.newsmanagement.dao.NewsDao#addNewsTagList(java.lang.Long,
     *      java.util.List)
     */
    @Override
    public void addNewsTagList(Long newsId, List<Long> tagsId) throws DaoException {
	Connection connection = DataSourceUtils.getConnection(dataSource);
	try (PreparedStatement preparedStatement = connection.prepareStatement(SQL_ADD_NEWS_TAG)) {
	    connection.setAutoCommit(false);
	    Iterator<Long> iterator = tagsId.iterator();
	    Long tagId;
	    while (iterator.hasNext()) {
		tagId = iterator.next();
		preparedStatement.setLong(1, newsId);
		preparedStatement.setLong(2, tagId);
		preparedStatement.addBatch();
	    }
	    preparedStatement.executeBatch();
	} catch (SQLException e) {
	    throw new DaoException("SQLException occurred while a news tag was being saved to the database", e);
	} finally {
	    DataSourceUtils.releaseConnection(connection, dataSource);
	}
    }

    /**
     * 
     * @see com.epam.newsmanagement.dao.NewsDao#deleteNewsAuthorByNewsId(java.lang.Long)
     */
    @Override
    public void deleteNewsAuthorByNewsId(Long newsId) throws DaoException {
	Connection connection = DataSourceUtils.getConnection(dataSource);
	try (PreparedStatement preparedStatement = connection.prepareStatement(SQL_DELETE_NEWS_AUTHOR_BY_NEWS_ID)) {
	    preparedStatement.setLong(1, newsId);
	    preparedStatement.executeUpdate();
	} catch (SQLException e) {
	    throw new DaoException(
		    "SQLException occurred while a news author was being deleted by a news id in the database", e);
	} finally {
	    DataSourceUtils.releaseConnection(connection, dataSource);
	}
    }

    /**
     * 
     * @see com.epam.newsmanagement.dao.NewsDao#updateNewsAuthorByNewsId(java.lang.Long)
     */
    @Override
    public void updateNewsAuthorByNewsId(Long authorId, Long newsId) throws DaoException {
	Connection connection = DataSourceUtils.getConnection(dataSource);
	try (PreparedStatement preparedStatement = connection.prepareStatement(SQL_UPDATE_NEWS_AUTHOR_BY_NEWS_ID)) {
	    preparedStatement.setLong(1, authorId);
	    preparedStatement.setLong(2, newsId);
	    preparedStatement.executeUpdate();
	} catch (SQLException e) {
	    throw new DaoException(
		    "SQLException occurred while a news author was being updated by a news id in the database", e);
	} finally {
	    DataSourceUtils.releaseConnection(connection, dataSource);
	}
    }

    /**
     * 
     * @see com.epam.newsmanagement.dao.NewsDao#deleteNewsTagByNewsIdList(java.lang.Long)
     */
    @Override
    public void deleteNewsAuthorByNewsIdList(List<Long> newsIdList) throws DaoException {
	Connection connection = DataSourceUtils.getConnection(dataSource);
	StringBuilder query = new StringBuilder(SQL_DELETE_NEWS_AUTHOR_BY_NEWS_ID_LIST);
	Iterator<Long> iterator = newsIdList.iterator();
	while (iterator.hasNext()) {
	    query.append(iterator.next());
	    if (iterator.hasNext()) {
		query.append(", ");
	    } else {
		query.append(")");
	    }
	}
	try (Statement statement = connection.createStatement()) {
	    statement.executeQuery(query.toString());
	} catch (SQLException e) {
	    throw new DaoException(
		    "SQLException occurred while authors was being deleted by a list of news id from the database", e);
	} finally {
	    DataSourceUtils.releaseConnection(connection, dataSource);
	}
    }

    /**
     * 
     * @see com.epam.newsmanagement.dao.NewsDao#deleteNewsTagByNewsId(java.lang.Long)
     */
    @Override
    public void deleteNewsTagByNewsId(Long newsId) throws DaoException {
	Connection connection = DataSourceUtils.getConnection(dataSource);
	try (PreparedStatement preparedStatement = connection.prepareStatement(SQL_DELETE_NEWS_TAG)) {
	    preparedStatement.setLong(1, newsId);
	    preparedStatement.executeUpdate();
	} catch (SQLException e) {
	    throw new DaoException(
		    "SQLException occurred while a news tag was being deleted by a news id in the database", e);
	} finally {
	    DataSourceUtils.releaseConnection(connection, dataSource);
	}
    }

    /**
     * 
     * @see com.epam.newsmanagement.dao.NewsDao#deleteNewsTagByNewsIdList(java.lang.Long)
     */
    @Override
    public void deleteNewsTagByNewsIdList(List<Long> newsIdList) throws DaoException {
	Connection connection = DataSourceUtils.getConnection(dataSource);
	StringBuilder query = new StringBuilder(SQL_DELETE_NEWS_TAG_BY_NEWS_ID_LIST);
	Iterator<Long> iterator = newsIdList.iterator();
	while (iterator.hasNext()) {
	    query.append(iterator.next());
	    if (iterator.hasNext()) {
		query.append(", ");
	    } else {
		query.append(")");
	    }
	}
	try (Statement statement = connection.createStatement()) {
	    statement.executeQuery(query.toString());
	} catch (SQLException e) {
	    throw new DaoException(
		    "SQLException occurred while tags was being deleted by a list of news id from the database", e);
	} finally {
	    DataSourceUtils.releaseConnection(connection, dataSource);
	}
    }

    /**
     * 
     * @see com.epam.newsmanagement.dao.NewsDao#deleteCommentsByNewsId(java.lang.Long)
     */
    @Override
    public void deleteCommentsByNewsIdList(List<Long> newsIdList) throws DaoException {
	Connection connection = DataSourceUtils.getConnection(dataSource);
	StringBuilder query = new StringBuilder(SQL_DELETE_COMMENTS_BY_NEWS_ID_LIST);
	Iterator<Long> iterator = newsIdList.iterator();
	while (iterator.hasNext()) {
	    query.append(iterator.next());
	    if (iterator.hasNext()) {
		query.append(", ");
	    } else {
		query.append(")");
	    }
	}
	try (Statement statement = connection.createStatement()) {
	    statement.executeQuery(query.toString());
	} catch (SQLException e) {
	    throw new DaoException(
		    "SQLException occurred while comments was being deleted by a list of news id from the database", e);
	} finally {
	    DataSourceUtils.releaseConnection(connection, dataSource);
	}
    }

    /**
     * 
     * @see com.epam.newsmanagement.dao.NewsDao#findNewsBySearchCriteria(com.epam.newsmanagement.entity.SearchCriteria)
     */
    @Override
    public List<News> findNewsBySearchCriteria(SearchCriteria searchCriteria) throws DaoException {
	Connection connection = DataSourceUtils.getConnection(dataSource);
	String query = buildSearchQuery(searchCriteria);
	List<News> newsList = new ArrayList<>();
	try (Statement statement = connection.createStatement()) {
	    ResultSet resultSet = statement.executeQuery(query);
	    while (resultSet.next()) {
		newsList.add(parseResultSet(resultSet));
	    }
	} catch (SQLException e) {
	    throw new DaoException(
		    "SQLException occurred while news was being found by a search criteria in the database", e);
	} finally {
	    DataSourceUtils.releaseConnection(connection, dataSource);
	}
	return newsList;
    }

    /**
     * 
     * @see com.epam.newsmanagement.dao.NewsDao#countAllNews()
     */
    @Override
    public Long countAllNews() throws DaoException {
	Connection connection = DataSourceUtils.getConnection(dataSource);
	Long newsQuantity = null;
	try (Statement statement = connection.createStatement()) {
	    ResultSet resultSet = statement.executeQuery(SQL_COUNT_ALL_NEWS);
	    if (resultSet.next()) {
		newsQuantity = resultSet.getLong(1);
	    }
	} catch (SQLException e) {
	    throw new DaoException(
		    "SQLException occurred while news was being found by a search criteria in the database", e);
	} finally {
	    DataSourceUtils.releaseConnection(connection, dataSource);
	}
	return newsQuantity;
    }

    /**
     * 
     * @see com.epam.newsmanagement.dao.NewsDao#findAndCountBySearchCriteria(com.epam.newsmanagement.entity.SearchCriteria)
     */
    @Override
    public int findAndCountNewsBySearchCriteria(SearchCriteria searchCriteria) throws DaoException {
	Connection connection = DataSourceUtils.getConnection(dataSource);
	String query = buildSearchQuery(searchCriteria);
	List<News> newsList = new ArrayList<>();
	try (Statement statement = connection.createStatement()) {
	    ResultSet resultSet = statement.executeQuery(query);
	    while (resultSet.next()) {
		newsList.add(parseResultSet(resultSet));
	    }
	} catch (SQLException e) {
	    throw new DaoException(
		    "SQLException occurred while news was being found and count by a search criteria in the database",
		    e);
	} finally {
	    DataSourceUtils.releaseConnection(connection, dataSource);
	}
	return newsList.size();
    }

    /**
     * builds a search query
     *
     * @param searchCriteria
     *            the search criteria
     * @return builded query
     */
    private String buildSearchQuery(SearchCriteria searchCriteria) {
	StringBuilder query = new StringBuilder(SQL_SEARCH_NEWS);
	Long authorId = searchCriteria.getAuthorId();
	List<Long> tagsId = new ArrayList<>();
	tagsId = searchCriteria.getTagsId();
	boolean hasAuthor = authorId != null;
	boolean hasTag = (tagsId!=null && !tagsId.isEmpty());
	if (hasAuthor && hasTag) {
	    query.append(SQL_COMPLEX_SEARCH_NEWS);
	    query.append(authorId);
	    query.append(" AND");
	    query.append(buildTagsListQuery(tagsId));
	}else if (hasAuthor) {
	    query.append(SQL_COMPLEX_SEARCH_NEWS_AUTHOR);
	    query.append(authorId);   
	} else if (hasTag) {    
	    query.append(SQL_COMPLEX_SEARCH_NEWS_TAG);
	    query.append(buildTagsListQuery(tagsId));
	}
	return query.toString();
    }

    /**
     * builds a query using a list with tags id
     * 
     * @param tagsId
     * @return builded query
     */
    private String buildTagsListQuery(List<Long> tagsId){
	StringBuilder query = new StringBuilder();
	Iterator<Long> iterator = tagsId.iterator();
	    query.append(" TAG_ID IN (");
	    while (iterator.hasNext()) {
		query.append(iterator.next());
		if (iterator.hasNext()) {
		    query.append(", ");
		} else {
		    query.append(")");
		}
	    } 
	    return query.toString();
    }
    
    /**
     * parses a set with dates about news.
     *
     * @param resultSet
     * @return a list with news
     * @throws DaoException
     */
    private News parseResultSet(ResultSet resultSet) throws DaoException {
	News news = new News();
	try {
	    news.setNewsId(resultSet.getLong(NEWS_ID));
	    news.setTitle(resultSet.getString(NEWS_TITLE));
	    news.setShortText(resultSet.getString(NEWS_SHORT_TEXT));
	    news.setFullText(resultSet.getString(NEWS_FULL_TEXT));
	    news.setCreationDate(resultSet.getTimestamp(NEWS_CREATION_DATE));
	    news.setModificationDate(resultSet.getDate(NEWS_MODIFICATION_DATE));
	} catch (SQLException e) {
	    throw new DaoException("SQLException occurred while was being parsed a result set for news", e);
	}
	return news;
    }
}
