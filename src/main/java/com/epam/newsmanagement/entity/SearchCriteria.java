package com.epam.newsmanagement.entity;

import java.util.ArrayList;
import java.util.List;

/**
 * The Class SearchCriteria.
 * 
 * The class contains search criteria to searching news
 */
public class SearchCriteria {
    private Long authorId;
    private List<Long> tagsId = new ArrayList<>();

    public Long getAuthorId() {
	return authorId;
    }

    public void setAuthorId(Long authorId) {
	this.authorId = authorId;
    }

    public List<Long> getTagsId() {
	return tagsId;
    }

    public void setTagsId(List<Long> tagsId) {
	this.tagsId = tagsId;
    }

    public void addTagId(Long tagId) {
	tagsId.add(tagId);
    }
}
