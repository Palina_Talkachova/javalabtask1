package com.epam.newsmanagement.entity;

import java.io.Serializable;
import java.sql.Date;
import java.sql.Timestamp;

/**
 * The Class News.
 */
public class News implements Serializable {
    private static final long serialVersionUID = 1L;
    private Long newsId;
    private String title;
    private String shortText;
    private String fullText;
    private Timestamp creationDate;
    private Date modificationDate;

    public News() {
    }

    public Long getNewsId() {
	return newsId;
    }

    public void setNewsId(Long newsId) {
	this.newsId = newsId;
    }

    public String getTitle() {
	return title;
    }

    public void setTitle(String title) {
	this.title = title;
    }

    public String getShortText() {
	return shortText;
    }

    public void setShortText(String shortText) {
	this.shortText = shortText;
    }

    public String getFullText() {
	return fullText;
    }

    public void setFullText(String fullText) {
	this.fullText = fullText;
    }

    public Timestamp getCreationDate() {
	return creationDate;
    }

    public void setCreationDate(Timestamp creationDate) {
	this.creationDate = creationDate;
    }

    public Date getModificationDate() {
	return modificationDate;
    }

    public void setModificationDate(Date modificationDate) {
	this.modificationDate = modificationDate;
    }

    @Override
    public int hashCode() {
	final int prime = 31;
	int result = 1;
	result = prime * result + ((creationDate == null) ? 0 : creationDate.hashCode());
	result = prime * result + ((fullText == null) ? 0 : fullText.hashCode());
	result = prime * result + ((modificationDate == null) ? 0 : modificationDate.hashCode());
	result = prime * result + ((newsId == null) ? 0 : newsId.hashCode());
	result = prime * result + ((shortText == null) ? 0 : shortText.hashCode());
	result = prime * result + ((title == null) ? 0 : title.hashCode());
	return result;
    }

    @Override
    public boolean equals(Object obj) {
	if (this == obj)
	    return true;
	if (obj == null)
	    return false;
	if (getClass() != obj.getClass())
	    return false;
	News other = (News) obj;
	if (creationDate == null) {
	    if (other.creationDate != null)
		return false;
	} else if (!creationDate.equals(other.creationDate))
	    return false;
	if (fullText == null) {
	    if (other.fullText != null)
		return false;
	} else if (!fullText.equals(other.fullText))
	    return false;
	if (modificationDate == null) {
	    if (other.modificationDate != null)
		return false;
	} else if (!modificationDate.equals(other.modificationDate))
	    return false;
	if (newsId == null) {
	    if (other.newsId != null)
		return false;
	} else if (!newsId.equals(other.newsId))
	    return false;
	if (shortText == null) {
	    if (other.shortText != null)
		return false;
	} else if (!shortText.equals(other.shortText))
	    return false;
	if (title == null) {
	    if (other.title != null)
		return false;
	} else if (!title.equals(other.title))
	    return false;
	return true;
    }

    @Override
    public String toString() {
	StringBuilder builder = new StringBuilder();
	builder.append("News [newsId=");
	builder.append(newsId);
	builder.append(", title=");
	builder.append(title);
	builder.append(", shortText=");
	builder.append(shortText);
	builder.append(", fullText=");
	builder.append(fullText);
	builder.append(", creationDate=");
	builder.append(creationDate);
	builder.append(", modificationDate=");
	builder.append(modificationDate);
	builder.append("]");
	return builder.toString();
    }
}