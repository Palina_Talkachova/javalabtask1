package com.epam.newsmanagement.entity;

import java.io.Serializable;
import java.sql.Timestamp;

/**
 * The Class Author.
 */
public class Author implements Serializable {
    private static final long serialVersionUID = 1L;
    private Long authorId;
    private String authorName;
    private Timestamp expired;

    public Author() {
    }

    public Long getAuthorId() {
	return authorId;
    }

    public void setAuthorId(Long authorId) {
	this.authorId = authorId;
    }

    public String getAuthorName() {
	return authorName;
    }

    public void setAuthorName(String authorName) {
	this.authorName = authorName;
    }

    public Timestamp getExpired() {
	return expired;
    }

    public void setExpired(Timestamp expired) {
	this.expired = expired;
    }

   
    @Override
    public int hashCode() {
	final int prime = 31;
	int result = 1;
	result = prime * result + ((authorId == null) ? 0 : authorId.hashCode());
	result = prime * result + ((authorName == null) ? 0 : authorName.hashCode());
	result = prime * result + ((expired == null) ? 0 : expired.hashCode());
	return result;
    }

    @Override
    public boolean equals(Object obj) {
	if (this == obj)
	    return true;
	if (obj == null)
	    return false;
	if (getClass() != obj.getClass())
	    return false;
	Author other = (Author) obj;
	if (authorId == null) {
	    if (other.authorId != null)
		return false;
	} else if (!authorId.equals(other.authorId))
	    return false;
	if (authorName == null) {
	    if (other.authorName != null)
		return false;
	} else if (!authorName.equals(other.authorName))
	    return false;
	if (expired == null) {
	    if (other.expired != null)
		return false;
	} else if (!expired.equals(other.expired))
	    return false;
	return true;
    }

    @Override
    public String toString() {
	StringBuilder builder = new StringBuilder();
	builder.append("Author [authorId=");
	builder.append(authorId);
	builder.append(", authorName=");
	builder.append(authorName);
	builder.append(", expired=");
	builder.append(expired);
	builder.append("]");
	return builder.toString();
    }
}