package com.epam.newsmanagement.entity;

import java.io.Serializable;

/**
 * The Class Tag.
 */
public class Tag implements Serializable {
    private static final long serialVersionUID = 1L;
    private Long tagId;
    private String tagName;

    public Tag() {
    }

    public Long getTagId() {
	return tagId;
    }

    public void setTagId(Long tagId) {
	this.tagId = tagId;
    }

    public String getTagName() {
	return tagName;
    }

    public void setTagName(String tagName) {
	this.tagName = tagName;
    }

    @Override
    public int hashCode() {
	final int prime = 31;
	int result = 1;
	result = prime * result + ((tagId == null) ? 0 : tagId.hashCode());
	result = prime * result + ((tagName == null) ? 0 : tagName.hashCode());
	return result;
    }

    @Override
    public boolean equals(Object obj) {
	if (this == obj)
	    return true;
	if (obj == null)
	    return false;
	if (getClass() != obj.getClass())
	    return false;
	Tag other = (Tag) obj;
	if (tagId == null) {
	    if (other.tagId != null)
		return false;
	} else if (!tagId.equals(other.tagId))
	    return false;
	if (tagName == null) {
	    if (other.tagName != null)
		return false;
	} else if (!tagName.equals(other.tagName))
	    return false;
	return true;
    }

    @Override
    public String toString() {
	StringBuilder builder = new StringBuilder();
	builder.append("Tag [tagId=");
	builder.append(tagId);
	builder.append(", tagName=");
	builder.append(tagName);
	builder.append("]");
	return builder.toString();
    }
}
