package com.epam.newsmanagement.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.epam.newsmanagement.dao.TagDao;
import com.epam.newsmanagement.dao.exception.DaoException;
import com.epam.newsmanagement.entity.Tag;
import com.epam.newsmanagement.service.TagService;
import com.epam.newsmanagement.service.exception.ServiceException;

/**
 * The Class TagServiceImpl.
 */
@Service
public class TagServiceImpl implements TagService {
    private static final Logger LOGGER = Logger.getLogger(TagServiceImpl.class);
    @Autowired
    private TagDao tagDao;

    /**
     * 
     * @see com.epam.newsmanagement.service.Service#add(java.lang.Object)
     */
    @Override
    public Long add(Tag tag) throws ServiceException {
	Long tagId;
	try {
	    tagId = tagDao.add(tag);
	} catch (DaoException e) {
	    LOGGER.error("Exception occurred while a tag was being saved to the database", e);
	    throw new ServiceException(e);

	}
	return tagId;
    }

    /**
     * 
     * @see com.epam.newsmanagement.service.Service#takeById(java.lang.Long)
     */
    @Override
    public Tag takeById(Long tagId) throws ServiceException {
	Tag tag;
	try {
	    tag = tagDao.takeById(tagId);
	} catch (DaoException e) {
	    LOGGER.error("Exception occurred while a tag was being updated in the database", e);
	    throw new ServiceException(e);
	}
	return tag;
    }

    /**
     * 
     * @see com.epam.newsmanagement.service.Service#update(java.lang.Object)
     */
    @Override
    public void update(Tag tag) throws ServiceException {
	try {
	    tagDao.update(tag);
	} catch (DaoException e) {
	    LOGGER.error("Exception occurred while a tag was being updated in the database", e);
	    throw new ServiceException(e);
	}
    }

    /**
     * 
     * @see com.epam.newsmanagement.service.Service#deleteById(java.lang.Long)
     */
    @Override
    public void deleteById(Long tagId) throws ServiceException {
	try {
	    tagDao.deleteById(tagId);
	} catch (DaoException e) {
	    LOGGER.error("Exception occurred while a tag was being deleted from the database", e);
	    throw new ServiceException(e);
	}
    }

    /**
     * 
     * @see com.epam.newsmanagement.service.Service#takeAll()
     */
    @Override
    public List<Tag> takeAll() throws ServiceException {
	List<Tag> tags = new ArrayList<>();
	try {
	    tags = tagDao.takeAll();
	} catch (DaoException e) {
	    LOGGER.error("Exception occurred while all tags was being got from the database", e);
	    throw new ServiceException(e);
	}
	return tags;
    }

    /**
     * 
     * @see com.epam.newsmanagement.service.TagService#takeByNewsId(java.lang.Long)
     */
    @Override
    public List<Tag> takeByNewsId(Long newsId) throws ServiceException {
	List<Tag> tags;
	try {
	    tags = tagDao.takeByNewsId(newsId);
	} catch (DaoException e) {
	    LOGGER.error("Exception occurred while tags was being got by a news id from the database", e);
	    throw new ServiceException(e);
	}
	return tags;
    }
}
