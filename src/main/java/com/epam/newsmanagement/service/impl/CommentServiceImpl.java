package com.epam.newsmanagement.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.epam.newsmanagement.dao.CommentDao;
import com.epam.newsmanagement.dao.exception.DaoException;
import com.epam.newsmanagement.entity.Comment;
import com.epam.newsmanagement.service.CommentService;
import com.epam.newsmanagement.service.exception.ServiceException;

/**
 * The Class CommentServiceImpl.
 */
@Service
public class CommentServiceImpl implements CommentService {
    private static final Logger LOGGER = Logger.getLogger(CommentServiceImpl.class);
    @Autowired
    private CommentDao commentDao;

    /**
     * 
     * @see com.epam.newsmanagement.service.Service#add(java.lang.Object)
     */
    @Override
    public Long add(Comment comment) throws ServiceException {
	Long commentId; 
	try {
	    commentId = commentDao.add(comment);
	} catch (DaoException e) {
	    LOGGER.error("Exception occurred while a comment was being saved to the database", e);
	    throw new ServiceException(e);
	}
	return commentId;
    }

    /**
     * 
     * @see com.epam.newsmanagement.service.Service#takeById(java.lang.Long)
     */
    @Override
    public Comment takeById(Long commentId
	    ) throws ServiceException {
	Comment comment;
	try {
	    comment = commentDao.takeById(commentId);
	} catch (DaoException e) {
	    LOGGER.error("Exception occurred while a comment was being got by id from the database", e);
	    throw new ServiceException(e);
	}
	return comment;
    }

    /**
     * 
     * @see com.epam.newsmanagement.service.Service#update(java.lang.Object)
     */
    @Override
    public void update(Comment comment) throws ServiceException {
	try {
	    commentDao.update(comment);
	} catch (DaoException e) {
	    LOGGER.error("Exception occurred while a comment was being updated in the database", e);
	    throw new ServiceException(e);
	}
    }

    /**
     * 
     * @see com.epam.newsmanagement.service.Service#deleteById(java.lang.Long)
     */
    @Override
    public void deleteById(Long commentId) throws ServiceException {
	try {
	    commentDao.deleteById(commentId);
	} catch (DaoException e) {
	    LOGGER.error("Exception occurred while a comment was being deleted from the database", e);
	    throw new ServiceException(e);
	}
    }

    /**
     * 
     * @see com.epam.newsmanagement.service.Service#takeAll()
     */
    @Override
    public List<Comment> takeAll() throws ServiceException {
	List<Comment> comments = new ArrayList<>();
	try {
	    comments = commentDao.takeAll();
	} catch (DaoException e) {
	    LOGGER.error("Exception occurred while all comments was being got from the database", e);
	    throw new ServiceException(e);
	}
	return comments;
    }

    /**
     * 
     * @see com.epam.newsmanagement.service.CommentService#takeById(java.lang.Long)
     */
    @Override
    public List<Comment> takeByNewsId(Long newsId) throws ServiceException {
	List<Comment> comments;
	try {
	    comments = commentDao.takeByNewsId(newsId);
	} catch (DaoException e) {
	    LOGGER.error("Exception occurred while comments was being got by news id from the database", e);
	    throw new ServiceException(e);
	}
	return comments;
    }
}
