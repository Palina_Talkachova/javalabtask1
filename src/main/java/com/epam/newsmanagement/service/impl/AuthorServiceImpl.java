package com.epam.newsmanagement.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.epam.newsmanagement.dao.AuthorDao;
import com.epam.newsmanagement.dao.exception.DaoException;
import com.epam.newsmanagement.entity.Author;
import com.epam.newsmanagement.service.AuthorService;
import com.epam.newsmanagement.service.exception.ServiceException;

/**
 * The Class AuthorServiceImpl.
 */
@Service
public class AuthorServiceImpl implements AuthorService {
    private static final Logger LOGGER = Logger.getLogger(AuthorServiceImpl.class);
    @Autowired
    private AuthorDao authorDao;

    /**
     * 
     * @see com.epam.newsmanagement.service.Service#add(java.lang.Object)
     */
    @Override
    public Long add(Author author) throws ServiceException {
	Long authorId;
	try {
	    authorId = authorDao.add(author);
	} catch (DaoException e) {
	    LOGGER.error("Exception occurred while an auhtor was being saved to the database", e);
	    throw new ServiceException(e);
	}
	return authorId;
    }

    /**
     * 
     * @see com.epam.newsmanagement.service.Service#takeById(java.lang.Long)
     */
    @Override
    public Author takeById(Long authorId) throws ServiceException {
	Author author;
	try {
	    author = authorDao.takeById(authorId);
	} catch (DaoException e) {
	    LOGGER.error("Exception occurred while an auhtor was being got by id from the database", e);
	    throw new ServiceException(e);
	}
	return author;
    }

    /**
     * 
     * @see com.epam.newsmanagement.service.Service#update(java.lang.Object)
     */
    @Override
    public void update(Author author) throws ServiceException {
	try {
	    authorDao.update(author);
	} catch (DaoException e) {
	    LOGGER.error("Exception occurred while an auhtor was being updated in the database", e);
	    throw new ServiceException(e);
	}
    }

    @Override
    public void deleteById(Long authorId) {
	throw new UnsupportedOperationException("The method to delete an author by id is in developing");
    }

    /**
     * 
     * @see com.epam.newsmanagement.service.Service#takeAll()
     */
    @Override
    public List<Author> takeAll() throws ServiceException {
	List<Author> authors = new ArrayList<>();
	try {
	    authors = authorDao.takeAll();
	} catch (DaoException e) {
	    LOGGER.error("Exception occurred while all auhtors was being got from the database", e);
	    throw new ServiceException(e);
	}
	return authors;
    }

    /**
     * 
     * @see com.epam.newsmanagement.service.AuthorService#expireAuthor(com.epam.newsmanagement.entity.Author)
     */
    @Override
    public void expireAuthor(Author author) throws ServiceException {
	try {
	    authorDao.expireAuthor(author);
	} catch (DaoException e) {
	    LOGGER.error("Exception occurred while an auhtor was being expired in the database", e);
	    throw new ServiceException(e);
	}
    }

    /**
     * 
     * @see com.epam.newsmanagement.service.AuthorService#takeByNewsId(java.lang.Long)
     */
    @Override
    public Author takeByNewsId(Long newsId) throws ServiceException {
	Author author;
	try {
	    author = authorDao.takeByNewsId(newsId);
	} catch (DaoException e) {
	    LOGGER.error("Exception occurred while an auhtor was being got by news id from the database", e);
	    throw new ServiceException(e);
	}
	return author;
    }
}
