package com.epam.newsmanagement.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.epam.newsmanagement.dao.NewsDao;
import com.epam.newsmanagement.dao.exception.DaoException;
import com.epam.newsmanagement.entity.News;
import com.epam.newsmanagement.entity.SearchCriteria;
import com.epam.newsmanagement.service.NewsService;
import com.epam.newsmanagement.service.exception.ServiceException;

/**
 * The Class NewsServiceImpl.
 */
@Service
public class NewsServiceImpl implements NewsService {
    private static final Logger LOGGER = Logger.getLogger(NewsServiceImpl.class);
    @Autowired
    private NewsDao newsDao;

    /**
     * 
     * @see com.epam.newsmanagement.service.Service#add(java.lang.Object)
     */
    @Override
    public Long add(News news) throws ServiceException {
	Long newsId;
	try {
	    newsId = newsDao.add(news);
	} catch (DaoException e) {
	    LOGGER.error("Exception occurred while a news was being saved to the database", e);
	    throw new ServiceException(e);
	}
	return newsId;
    }

    /**
     * 
     * @see com.epam.newsmanagement.service.Service#takeById(java.lang.Long)
     */
    @Override
    public News takeById(Long newsId) throws ServiceException {
	News news;
	try {
	    news = newsDao.takeById(newsId);
	} catch (DaoException e) {
	    LOGGER.error("Exception occurred while a news was being got by id from the database", e);
	    throw new ServiceException(e);
	}
	return news;
    }

    /**
     * 
     * @see com.epam.newsmanagement.service.Service#update(java.lang.Object)
     */
    @Override
    public void update(News news) throws ServiceException {
	try {
	    newsDao.update(news);
	} catch (DaoException e) {
	    LOGGER.error("Exception occurred while a news was being updated in the database", e);
	    throw new ServiceException(e);
	}
    }

    /**
     * 
     * @see com.epam.newsmanagement.service.Service#deleteById(java.lang.Long)
     */
    @Override
    public void deleteById(Long newsId) throws ServiceException {
	try {
	    newsDao.deleteById(newsId);
	} catch (DaoException e) {
	    LOGGER.error("Exception occurred while a news was being deleted from the database", e);
	    throw new ServiceException(e);
	}
    }

    public void deleteNewsByNewsIdList(List<Long> newsIdList) throws ServiceException {
	try {
	    newsDao.deleteNewsByNewsIdList(newsIdList);
	} catch (DaoException e) {
	    LOGGER.error("Exception occurred while news was being deleted by a list of news id from the database", e);
	    throw new ServiceException(e);
	}
    }

    /**
     * 
     * @see com.epam.newsmanagement.service.Service#takeAll()
     */
    @Override
    public List<News> takeAll() throws ServiceException {
	List<News> news = new ArrayList<>();
	try {
	    news = newsDao.takeAll();
	} catch (DaoException e) {
	    LOGGER.error("Exception occurred while all news was being got from the database", e);
	    throw new ServiceException(e);
	}
	return news;
    }

    /**
     * 
     * @see com.epam.newsmanagement.service.NewsService#takeByAuhtorId(java.lang.Long)
     */
    @Override
    public List<News> takeByAuhtorId(Long authorId) throws ServiceException {
	List<News> newsList;
	try {
	    newsList = newsDao.takeByAuhtorId(authorId);
	} catch (DaoException e) {
	    LOGGER.error("Exception occurred while news was being got by an author's id from the database", e);
	    throw new ServiceException(e);
	}
	return newsList;
    }

    /**
     * 
     * @see com.epam.newsmanagement.service.NewsService#takeByTagId(java.lang.Long)
     */
    @Override
    public List<News> takeByTagId(Long tagId) throws ServiceException {
	List<News> newsList;
	try {
	    newsList = newsDao.takeByTagId(tagId);
	} catch (DaoException e) {
	    LOGGER.error("Exception occurred while news was being got by tag id from the database", e);
	    throw new ServiceException(e);
	}
	return newsList;
    }

    /**
     * 
     * @see com.epam.newsmanagement.service.NewsService#addNewsAuthor(java.lang.Long,
     *      java.lang.Long)
     */
    @Override
    public void addNewsAuthor(Long newsId, Long authorId) throws ServiceException {
	try {
	    newsDao.addNewsAuthor(newsId, authorId);
	} catch (DaoException e) {
	    LOGGER.error("Exception occurred while a news author was being saved to the database", e);
	    throw new ServiceException(e);
	}
    }

    /**
     * 
     * @see com.epam.newsmanagement.service.NewsService#addNewsTag(java.lang.Long,
     *      java.lang.Long)
     */
    @Override
    public void addNewsTag(Long newsId, List<Long> tagsId) throws ServiceException {
	try {
	    newsDao.addNewsTagList(newsId, tagsId);
	} catch (DaoException e) {
	    LOGGER.error("Exception occurred while a news tag was being saved to the database", e);
	    throw new ServiceException(e);
	}
    }

    /**
     * 
     * @see com.epam.newsmanagement.service.NewsService#deleteNewsAuthorByNewsId(java.lang.Long)
     */
    @Override
    public void deleteNewsAuthorByNewsId(Long id) throws ServiceException {
	try {
	    newsDao.deleteNewsAuthorByNewsId(id);
	} catch (DaoException e) {
	    LOGGER.error("Exception occurred while a news author was being deleted by a news id in the database", e);
	    throw new ServiceException(e);
	}
    }

    /**
     * 
     * @see com.epam.newsmanagement.service.NewsService#updateNewsAuthorByNewsId(java.lang.Long)
     */
    @Override
    public void updateNewsAuthorByNewsId(Long authorId, Long newsId) throws ServiceException {
	try {
	    newsDao.updateNewsAuthorByNewsId(authorId, newsId);
	} catch (DaoException e) {
	    LOGGER.error("Exception occurred while a news author was being updated by a news id in the database", e);
	    throw new ServiceException(e);
	}
    }

    /**
     * 
     * @see com.epam.newsmanagement.service.NewsService#deleteNewsTagByNewsId(java.lang.Long)
     */
    @Override
    public void deleteNewsTagByNewsId(Long id) throws ServiceException {
	try {
	    newsDao.deleteNewsTagByNewsId(id);
	} catch (DaoException e) {
	    LOGGER.error("SQLException occurred while a news tag was being deleted by a news id in the database", e);
	    throw new ServiceException(e);
	}
    }

    @Override
    public void deleteNewsTagByNewsIdList(List<Long> newsIdList) throws ServiceException {
	try {
	    newsDao.deleteNewsTagByNewsIdList(newsIdList);
	} catch (DaoException e) {
	    LOGGER.error("SQLException occurred while tags was being deleted by a list of news id from the database",
		    e);
	    throw new ServiceException(e);
	}
    }

    /**
     * 
     * @see com.epam.newsmanagement.service.NewsService#deleteCommentsByNewsId(java.lang.Long)
     */
    @Override
    public void deleteCommentsByNewsIdList(List<Long> newsIdList) throws ServiceException {
	try {
	    newsDao.deleteCommentsByNewsIdList(newsIdList);
	} catch (DaoException e) {
	    LOGGER.error(
		    "SQLException occurred while comments was being deleted by a list of news id from the database", e);
	    throw new ServiceException(e);
	}
    }

    /**
     * 
     * @see com.epam.newsmanagement.service.deleteNewsAuthorByNewsIdList(java.
     *      lang.Long)
     */
    @Override
    public void deleteNewsAuthorByNewsIdList(List<Long> newsIdList) throws ServiceException {
	try {
	    newsDao.deleteNewsAuthorByNewsIdList(newsIdList);
	} catch (DaoException e) {
	    LOGGER.error("SQLException occurred while auhtors was being deleted by a liat of news id from the database",
		    e);
	    throw new ServiceException(e);
	}
    }

    /**
     * 
     * @see com.epam.newsmanagement.service.NewsService#findBySearchCriteria(com.epam.newsmanagement.entity.SearchCriteria)
     */
    @Override
    public List<News> findBySearchCriteria(SearchCriteria searchCriteria) throws ServiceException {
	List<News> news = new ArrayList<>();
	try {
	    newsDao.findNewsBySearchCriteria(searchCriteria);
	} catch (DaoException e) {
	    LOGGER.error("Exception occurred while news was being found by a search criteria in the database", e);
	    throw new ServiceException(e);
	}
	return news;
    }

    /**
     * 
     * @see com.epam.newsmanagement.service.NewsService#countAllNews()
     */
    @Override
    public Long countAllNews() throws ServiceException {
	Long newsQuantity;
	try {
	    newsQuantity = newsDao.countAllNews();
	} catch (DaoException e) {
	    LOGGER.error("Exception occurred while news was being counted in the database", e);
	    throw new ServiceException(e);
	}
	return newsQuantity;
    }

    public int countNewsBySearchCriteria(SearchCriteria searchCriteria) throws ServiceException {
	int newsQuantity;
	try {
	    newsQuantity = newsDao.findAndCountNewsBySearchCriteria(searchCriteria);
	} catch (DaoException e) {
	    LOGGER.error("Exception occurred while news was being counted according with a search criteria", e);
	    throw new ServiceException(e);
	}
	return newsQuantity;
    }
}
