package com.epam.newsmanagement.service.impl;

import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.epam.newsmanagement.entity.News;
import com.epam.newsmanagement.entity.NewsVO;
import com.epam.newsmanagement.service.AuthorService;
import com.epam.newsmanagement.service.CommentService;
import com.epam.newsmanagement.service.NewsHandlingService;
import com.epam.newsmanagement.service.NewsService;
import com.epam.newsmanagement.service.TagService;
import com.epam.newsmanagement.service.exception.ServiceException;

/**
 * The Class NewsHandlingServiceImpl.
 */

@Service
public class NewsHandlingServiceImpl implements NewsHandlingService {
    private static final Logger LOGGER = Logger.getLogger(NewsHandlingServiceImpl.class);
    @Autowired
    private NewsService newsService;
    @Autowired
    private AuthorService authorService;
    @Autowired
    private TagService tagService;
    @Autowired
    private CommentService commentService;

    /**
     * 
     * @see com.epam.newsmanagement.service.NewsHandlingService#takeSingleNews(java.
     *      lang.Long)
     */
    @Transactional(rollbackFor = ServiceException.class)
    @Override
    public NewsVO takeCompletedNews(Long newsId) throws ServiceException {
	NewsVO newsVO = new NewsVO();
	try {
	    newsVO.setNews(newsService.takeById(newsId));
	    newsVO.setAuthor(authorService.takeByNewsId(newsId));
	    newsVO.setTags(tagService.takeByNewsId(newsId));
	    newsVO.setComments(commentService.takeByNewsId(newsId));
	} catch (ServiceException e) {
	    LOGGER.error("Exception occurred while a single news was being taken from the database", e);
	    throw e;
	}
	return newsVO;
    }

    /**
     * 
     * @see com.epam.newsmanagement.service.NewsHandlingService#saveNews(com.epam.
     *      newsmanagement.entity.News, java.lang.Long, java.util.List)
     */
    @Transactional(rollbackFor = ServiceException.class)
    @Override
    public void saveNews(News news, Long authorId, List<Long> tags) throws ServiceException {
	try {
	    Long newsId = newsService.add(news);
	    newsService.addNewsAuthor(newsId, authorId);
	    newsService.addNewsTag(newsId, tags);
	} catch (ServiceException e) {
	    LOGGER.error("Exception occurred while a completed news was being saved to the database", e);
	    throw e;
	}
    }

    /**
     * 
     * @see com.epam.newsmanagement.service.NewsHandlingService#deleteListOfNews(java.util.List)
     */
    @Transactional(rollbackFor = ServiceException.class)
    @Override
    public void deleteListOfNews(List<Long> newsIdList) throws ServiceException {
	try {
	    newsService.deleteCommentsByNewsIdList(newsIdList);
	    newsService.deleteCommentsByNewsIdList(newsIdList);
	    newsService.deleteNewsTagByNewsIdList(newsIdList);
	    newsService.deleteNewsByNewsIdList(newsIdList);
	} catch (ServiceException e) {
	    LOGGER.error("Exception occurred while a completed news was being deleted from the database", e);
	    throw e;
	}
    }

    /**
     * 
     * @see com.epam.newsmanagement.service.NewsHandlingService#updateNews(com.epam.newsmanagement.entity.News,
     *      java.lang.Long, java.util.List)
     */
    @Transactional(rollbackFor = ServiceException.class)
    @Override
    public void updateNews(News news, Long authorId, List<Long> tags) throws ServiceException {
	try {
	    newsService.updateNewsAuthorByNewsId(authorId, news.getNewsId());
	    newsService.deleteNewsTagByNewsId(news.getNewsId());
	    newsService.update(news);
	    newsService.addNewsTag(news.getNewsId(), tags);
	} catch (ServiceException e) {
	    LOGGER.error("Exception occurred while a completed news was being updated in� the database", e);
	    throw e;
	}
    }
}
