package com.epam.newsmanagement.service;

import java.util.List;

import com.epam.newsmanagement.service.exception.ServiceException;

public interface Service<T> {

    /**
     * adds an entity to DB
     * 
     * @param t
     * @throws ServiceException
     */
    Long add(T t) throws ServiceException;

    /**
     * gets an entity by id
     * 
     * @param id
     * @return T
     * @throws ServiceException
     */
    T takeById(Long id) throws ServiceException;

    /**
     * updates an entity by id
     * 
     * @param t
     * @throws ServiceException
     */
    void update(T t) throws ServiceException;

    /**
     * deletes an entity by id
     * 
     * @param id
     * @throws ServiceException
     */
    void deleteById(Long id) throws ServiceException;

    /**
     * gets a list with entities
     * 
     * @return a list with entities
     * @throws ServiceException
     */
    List<T> takeAll() throws ServiceException;
}
