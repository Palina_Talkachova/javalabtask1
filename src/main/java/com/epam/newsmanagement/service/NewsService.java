package com.epam.newsmanagement.service;

import java.util.List;

import com.epam.newsmanagement.entity.News;
import com.epam.newsmanagement.entity.SearchCriteria;
import com.epam.newsmanagement.service.exception.ServiceException;

/**
 * The Interface NewsService.
 */
public interface NewsService extends Service<News> {

    /**
     * gets a list with news by author's id.
     *
     * @param authorId
     *            the id
     * @return a list with news
     * @throws ServiceException
     *             the service exception
     */
    List<News> takeByAuhtorId(Long authorId) throws ServiceException;

    /**
     * gets a list with news by tag id.
     *
     * @param tagId
     *            the id
     * @return a list with news
     * @throws ServiceException
     *             the service exception
     */
    List<News> takeByTagId(Long tagId) throws ServiceException;

    /**
     * adds an author for a news.
     *
     * @param newsId
     *            the news id
     * @param authorId
     *            the author id
     * @throws ServiceException
     *             the service exception
     */
    void addNewsAuthor(Long newsId, Long authorId) throws ServiceException;

    /**
     * adds a tag (tags) for a news.
     *
     * @param newsId
     *            the news id
     * @param tagId
     *            the tag id
     * @throws ServiceException
     *             the service exception
     */
    void addNewsTag(Long newsId, List<Long> tagsId) throws ServiceException;

    /**
     * deletes news by a list of news id
     * 
     * @param newsIdList
     * @throws ServiceException
     */
    void deleteNewsByNewsIdList(List<Long> newsIdList) throws ServiceException;
    
    /**
     * deletes a news author by a news id
     * 
     * @param id
     * @throws ServiceException
     */
    void deleteNewsAuthorByNewsId(Long id) throws ServiceException;
    
    /**
     * deletes news authors by a list of news id
     * 
     * @param id
     * @throws ServiceException
     */
    void deleteNewsAuthorByNewsIdList(List<Long> newsIdList) throws ServiceException;
    
    /**
     * updates a news author by a news id
     * 
     * @param authorId
     * @param newsId
     * @throws ServiceException
     */
    void updateNewsAuthorByNewsId(Long authorId, Long newsId) throws ServiceException;

    /**
     * deletes a news tag by a news id
     * 
     * @param id
     * @throws ServiceException
     */
    void deleteNewsTagByNewsId(Long id) throws ServiceException;
    
    /**
     * deletes news tags by a list of news id
     * 
     * @param newsIdList
     * @throws ServiceException
     */
    void deleteNewsTagByNewsIdList(List<Long> newsIdList) throws ServiceException;

    /**
     * deletes comments by a list of news id
     * 
     * @param id
     * @throws ServiceException
     */
    void deleteCommentsByNewsIdList(List<Long> newsIdList) throws ServiceException;

    /**
     * search news by a search criteria.
     *
     * @param searchCriteria
     *            the search criteria
     * @return a list with news
     * @throws ServiceException
     *             the service exception
     */
    List<News> findBySearchCriteria(SearchCriteria searchCriteria) throws ServiceException;

    /**
     * count all news
     *
     * @return the long
     * @throws ServiceException
     *             the service exception
     */
    Long countAllNews() throws ServiceException;
    
    /**
     * counts news had been obtained by a search criteria
     * 
     * @param searchCriteria
     * @return
     */
    int countNewsBySearchCriteria(SearchCriteria searchCriteria) throws ServiceException;
}
