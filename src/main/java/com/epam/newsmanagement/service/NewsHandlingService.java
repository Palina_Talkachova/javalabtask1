package com.epam.newsmanagement.service;

import java.util.List;

import com.epam.newsmanagement.entity.News;
import com.epam.newsmanagement.entity.NewsVO;
import com.epam.newsmanagement.service.exception.ServiceException;

public interface NewsHandlingService {

    /**
     * gets a single news by id
     * 
     * @param newsId
     * @return NewsVO
     * @throws ServiceException
     */
    NewsVO takeCompletedNews(Long newsId) throws ServiceException;

    /**
     * saves completed data about news in DB
     * 
     * @param news
     * @param authorId
     * @param tags
     * @throws ServiceException
     * @throws Exception 
     */
    void saveNews(News news, Long authorId, List<Long> tags) throws ServiceException;

    /**
     * deletes completed data about news in DB
     * 
     * @param newsIdList
     * @throws ServiceException
     */
    void deleteListOfNews(List<Long> newsIdList) throws ServiceException;

    /**
     * updates completed data about news in DB
     * 
     * @param newsIdList
     * @throws ServiceException
     */
    public void updateNews(News news, Long authorId, List<Long> tags) throws ServiceException;
}
