package com.epam.newsmanagement.service;

import java.util.List;

import com.epam.newsmanagement.entity.Tag;
import com.epam.newsmanagement.service.exception.ServiceException;

public interface TagService extends Service<Tag> {

    /**
     * gets a list with tags by a news id
     * 
     * @param newsId
     * @return a list with tags
     * @throws ServiceException
     */
    List<Tag> takeByNewsId(Long newsId) throws ServiceException;
}
