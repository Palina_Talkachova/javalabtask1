package com.epam.newsmanagement.service;

import com.epam.newsmanagement.entity.Author;
import com.epam.newsmanagement.service.exception.ServiceException;

public interface AuthorService extends Service<Author> {

    /**
     * expires an author
     * 
     * @param author
     * @throws ServiceException
     */
    void expireAuthor(Author author) throws ServiceException;

    /**
     * gets an author by a news id
     * 
     * @param newsId
     * @return Author
     * @throws ServiceException
     */
    Author takeByNewsId(Long newsId) throws ServiceException;
}
