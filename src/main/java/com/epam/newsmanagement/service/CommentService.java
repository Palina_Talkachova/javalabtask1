package com.epam.newsmanagement.service;

import java.util.List;

import com.epam.newsmanagement.entity.Comment;
import com.epam.newsmanagement.service.exception.ServiceException;

public interface CommentService extends Service<Comment> {

    /**
     * gets a list with comments by a news id
     * 
     * @param newsId
     * @return
     * @throws ServiceException
     */
    List<Comment> takeByNewsId(Long newsId) throws ServiceException;
}
