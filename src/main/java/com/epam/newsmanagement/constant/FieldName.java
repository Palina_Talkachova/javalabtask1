package com.epam.newsmanagement.constant;

/**
 * The class involves constants to describe fields in tables of DB
 */
public class FieldName {
    public static final String NEWS_ID = "NEWS_ID";
    public static final String NEWS_TITLE = "TITLE";
    public static final String NEWS_SHORT_TEXT = "SHORT_TEXT";
    public static final String NEWS_FULL_TEXT = "FULL_TEXT";
    public static final String NEWS_CREATION_DATE = "CREATION_DATE";
    public static final String NEWS_MODIFICATION_DATE = "MODIFICATION_DATE";
    public static final String AUTHOR_ID = "AUTHOR_ID";
    public static final String AUTHOR_NAME = "AUTHOR_NAME";
    public static final String AUTHOR_EXPIRED = "EXPIRED";
    public static final String TAG_ID = "TAG_ID";
    public static final String TAG_NAME = "TAG_NAME";
    public static final String COMMENT_ID = "COMMENT_ID";
    public static final String COMMENT_TEXT = "COMMENT_TEXT";
    public static final String COMMENT_CREATION_DATE = "CREATION_DATE";
    public static final String NEXTVAL = "NEXTVAL";
    public static final String COUNT = "COUNT(*)";
}
