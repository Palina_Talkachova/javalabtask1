
package com.epam.newsmanagement.run;

import java.sql.Date;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import com.epam.newsmanagement.config.ApplicationConfig;
import com.epam.newsmanagement.dao.exception.DaoException;
import com.epam.newsmanagement.entity.Author;
import com.epam.newsmanagement.entity.News;
import com.epam.newsmanagement.entity.Tag;
import com.epam.newsmanagement.service.AuthorService;
import com.epam.newsmanagement.service.NewsHandlingService;
import com.epam.newsmanagement.service.TagService;
import com.epam.newsmanagement.service.exception.ServiceException;

/**
 * The Class Runner.
 */
public class Runner {
    private static AuthorService authorService;
    private static TagService tagService;
    private static NewsHandlingService newsHandlingService;
	
	
    /**
     * The main method.
     *
     * @param args
     * @throws ServiceException
     * @throws DaoException
     */
    public static void main(String[] args) throws ServiceException, DaoException {
	@SuppressWarnings("resource")
	ApplicationContext context = new AnnotationConfigApplicationContext(ApplicationConfig.class);
	authorService = context.getBean(AuthorService.class);
	tagService = context.getBean(TagService.class);
	newsHandlingService = context.getBean(NewsHandlingService.class);
	addNews();

    }
	
 
    private static void addNews() throws ServiceException {
	try {
	    Tag tagOne = new Tag();
	    tagOne.setTagName("sport");
	    Author author = new Author();
	    author.setAuthorName("Jecky");
	    News news = new News();
	    news.setTitle("Title");
	    news.setShortText("Short text");
	    news.setFullText("Full text");
	    news.setCreationDate(Timestamp.valueOf("2015-09-09 10:10:30.11"));
	    news.setModificationDate(Date.valueOf("2015-09-09"));
	    List<Long> tags = new ArrayList<>();
	    tags.add(tagService.add(tagOne));
	    newsHandlingService.saveNews(news, authorService.add(author), tags);
	} catch (ServiceException e) {
	    e.printStackTrace();
	}
    }
}