package com.epam.newsmanagement.config;

import javax.sql.DataSource;

import org.apache.commons.dbcp2.BasicDataSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.core.env.Environment;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

/**
 * Declares methods to provide and manage the data source for DB.
 */
@EnableTransactionManagement
@ComponentScan("com.epam.newsmanagement.dao, com.epam.newsmanagement.service")
public class BaseApplicationConfig { 
    private static final String PROP_DATABASE_DRIVER = "db.driver";
    private static final String PROP_DATABASE_PASSWORD = "db.password";
    private static final String PROP_DATABASE_URL = "db.url";
    private static final String PROP_DATABASE_USERNAME = "db.username";
    private static final String PROP_DATABASE_POOL_SIZE = "db.initialsize";
    
    @Autowired
    private Environment environment;

    /**
     * gets the data source from property file.
     *
     * @return dataSource
     */
    @Bean
    public DataSource dataSource() {
	BasicDataSource dataSource = new BasicDataSource();
	dataSource.setDriverClassName(environment.getRequiredProperty(PROP_DATABASE_DRIVER));
	dataSource.setUrl(environment.getRequiredProperty(PROP_DATABASE_URL));
	dataSource.setUsername(environment.getRequiredProperty(PROP_DATABASE_USERNAME));
	dataSource.setPassword(environment.getRequiredProperty(PROP_DATABASE_PASSWORD));
	dataSource.setInitialSize(Integer.parseInt(environment.getRequiredProperty(PROP_DATABASE_POOL_SIZE)));
	return dataSource;
    }
    
    /**
     * manages transactions
     *
     * @return the platform transaction manager
     */
    @Bean
    public PlatformTransactionManager txManager() {
	return new DataSourceTransactionManager(dataSource());
    }
}



