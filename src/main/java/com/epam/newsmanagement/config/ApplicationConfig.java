package com.epam.newsmanagement.config;

import javax.sql.DataSource;

import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

/**
 * Declares methods to provide and manage the data source for DB.
 */
@Configuration
@PropertySource("classpath:db.properties")
public class ApplicationConfig extends BaseApplicationConfig {

    /**
     * 
     * @see com.epam.newsmanagement.config.BaseApplicationConfig#dataSource()
     */
    @Override
    public DataSource dataSource() {
	return super.dataSource();
    }  
}
